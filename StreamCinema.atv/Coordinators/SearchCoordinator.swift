//
//  SearchCoordinator.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 25.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit


final class SearchCoordinator: Coordinator {

    let rootViewController: UINavigationController
    private let appData: AppData

    // MARK: Coordinators
    private var movieShowsCoordinator: MovieCoordinator?

    init(appData: AppData, rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
        self.appData = appData
    }

    func start() { }

    func createVC(for tabBar: TabBarItem) -> UIViewController {
        let vc: UIViewController
        if tabBar == .search {
            vc = createAZSearchScreen()
        } else {
            #if os(tvOS)
            vc = createFullTextSearchScreen()
            #else
            let controller = createFullTextSearchScreen()
            vc = UINavigationController(rootViewController: controller)
            #endif
        }
        vc.tabBarItem.image = UIImage(systemName: "magnifyingglass")
        vc.tabBarItem.title = TabBarItem.fullTextSearch.description.capitalized
        vc.tabBarItem.tag = tabBar.rawValue
        return vc
    }

    func presentFullTextSearchScreen() {
        #if os(tvOS)
        let vc = createFullTextSearchScreen()
        rootViewController.pushViewController(vc, animated: true)
        #else
        let controller = createFullTextSearchScreen()
        let vc = UINavigationController(rootViewController: controller)
        rootViewController.pushViewController(vc, animated: true)
        #endif
        
    }

    private func presentMovieDetail(data: SCCMovie) {
        self.movieShowsCoordinator = MovieCoordinator(appData: appData, rootViewController: rootViewController)
        movieShowsCoordinator?.start()
        movieShowsCoordinator?.presentMovieAndShowViewController(movieData: data)
    }

    private func createAZSearchScreen() -> UIViewController {
        let vc = AZSearchController.create(appData: appData)
        vc.onPresentMovieDetailScreen = presentMovieDetail(data:)
        return vc
    }
    
    private func createFullTextSearchScreen() -> UISearchContainerViewController {
        let movieList = MovieListViewController.create(appData: appData)
        let vc = FullSearchController.create(appData: appData, searchResultController: movieList)
        vc.onPresentMovieDetailScreen = presentMovieDetail(data:)
        let container = UISearchContainerViewController(searchController: vc)
        return container
    }
}
