//
//  StudiosCoordinator.swift
//  StreamCinema.atv
//
//  Created by Alanko5 on 22/12/2022.
//  Copyright © 2022 SCC. All rights reserved.
//

import Foundation
import UIKit
import Combine

protocol StudiosCoordinatorDelegate: AnyObject {
    func presentCastDetailScreen(for cast: Cast)
    func presentDetailForCastScreen(for staff: MovieStaff)
    func presentMovieDetailScreen(for relatedMovie: SCCMovie)
    func presentVideoPlayer(model: MovieModel, isContinue: Bool, sourceView: UIView)
    func presentHDRPlayer(model: MovieModel)
}

final class StudiosCoordinator: Coordinator {
    weak var delegate: StudiosCoordinatorDelegate?
    
    var rootViewController: UINavigationController
    internal let appData: AppData
    internal let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    internal var cancelables: Set<AnyCancellable> = Set()
    internal var searchController: UISearchContainerViewController?
    internal var data: [AZResult] = [] {
        didSet {
            let studiosVC = searchController?.searchController.searchResultsUpdater as? StudiosViewController
            studiosVC?.data = data.sorted(by: { ($0.count ?? 0) > ($1.count ?? 0) })
        }
    }
    
    init(appData: AppData, rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
        self.appData = appData
        start()
    }
    
    func start() {
        appData.scService.getAllStudios(model: FilterModel.initStudios(startIndex: 0))
            .assignError(to: errorSubject)
            .sink(receiveCompletion: { complet in
                print(complet)
            }, receiveValue: { [weak self] results in
                self?.data = results.data ?? []
            })
            .store(in: &cancelables)
    }
    
    func createVC(for tabBar: TabBarItem) -> UIViewController {
        if searchController == nil {
            let studiosVC = StudiosViewController()
            let searchVC = UISearchController(searchResultsController: studiosVC)
            searchVC.searchResultsUpdater = studiosVC
            studiosVC.studioDelegate = self
            self.searchController = UISearchContainerViewController(searchController: searchVC)
            searchController?.title = tabBar.description
        }
        searchController?.tabBarItem.tag = tabBar.rawValue
        return searchController!
    }
}

extension StudiosCoordinator: MovieViewControllerDelegate, StudiosViewControllerDelegate {
    func presentStaffMoviesViewController(for staff: MovieStaff) {
        delegate?.presentDetailForCastScreen(for: staff)
    }
    
    func presentCastDetailScreen(for cast: Cast) {
        delegate?.presentCastDetailScreen(for: cast)
    }
    
    func presentDetailForCastScreen(for staff: MovieStaff) {
        delegate?.presentDetailForCastScreen(for: staff)
    }
    
    func presentMovieDetailScreen(for relatedMovie: SCCMovie) {
        delegate?.presentMovieDetailScreen(for: relatedMovie)
    }
    
    func presentVideoPlayer(model: MovieModel, isContinue: Bool, sourceView: UIView) {
        delegate?.presentVideoPlayer(model: model, isContinue: isContinue, sourceView: sourceView)
    }
    
    func presentHDRPlayer(model: MovieModel) {
        delegate?.presentHDRPlayer(model: model)
    }
}

