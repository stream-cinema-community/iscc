//
//  SettingsCoordinator.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 25.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

final class SettingsCoordinator: Coordinator {

    let rootViewController: UINavigationController
    private let appData: AppData

    private let settings: SettingsViewController = SettingsViewController(style: .grouped)
    private let menu: MenuViewController = MenuViewController()

    init(appData: AppData, rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
        self.appData = appData
    }

    func start() { }


    func createVC(for tabBar: TabBarItem) -> UIViewController {
        let vc = createSettingScreen()
        vc.tabBarItem.title = tabBar.description
        vc.tabBarItem.tag = tabBar.rawValue
        return vc
    }

    func presentSettingScreen() {
        let vc = createSettingScreen()
        rootViewController.pushViewController(vc, animated: true)
    }

    private func createSettingScreen() -> UISplitViewController {
        let viewModel = SettingsViewModel(appData: appData)
        let vc = SettingsViewController.create(viewModel: viewModel)
        let menu: MenuViewController = MenuViewController()
        menu.endpoit = TabBarItem.settings
        menu.menuDelegate = vc
        let split = UISplitViewController()
        split.viewControllers = [menu, vc]
        split.title = TabBarItem.settings.description
        split.preferredPrimaryColumnWidthFraction = 2/7
        return split
    }
    
//    private func createSettingScreen() -> UISplitViewController {
//        let viewModel = SettingsViewModel(appData: appData)
//        let vc = SettingsViewController.create(viewModel: viewModel)
//        let menu = SettingsLeftSideController()
//        vc.settingsDelegate = menu
////        menu.endpoit = TabBarItem.settings
////        menu.menuDelegate = vc
//        let split = UISplitViewController()
//        split.viewControllers = [menu, vc]
//        split.title = TabBarItem.settings.description
//        split.preferredPrimaryColumnWidthFraction = 3/7
//        return split
//    }

}

final class SettingsLeftSideController: UIViewController {
    internal let imageView = UIImageView()
    internal let descLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }
    
    internal func configureViews() {
        view.addSubview(imageView)
        view.addSubview(descLabel)
        
        descLabel.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        descLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -32).isActive = true
        descLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        descLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        descLabel.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 32).isActive = true
        imageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32).isActive = true
        imageView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -32).isActive = true
        imageView.bottomAnchor.constraint(equalTo: descLabel.topAnchor, constant: -16).isActive = true
        
        descLabel.textAlignment = .center
        imageView.contentMode = .scaleAspectFit
    }
}

extension SettingsLeftSideController: SettingsViewControllerDelegate {
    func settingsController(_ viewController: SettingsViewController, didFocus item: SettingsCellTypes) {
        imageView.image = item.image
        descLabel.text = item.desc
    }
}
