//
//  CastCell.swift
//  StreamCinema.atv
//
//  Created by SCC on 21/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

#if canImport(TVUIKit)
import TVUIKit
#else
import UIKit

final class TVMonogramView: UIView {
    internal var titleLabel = UILabel()
    internal var subtitleLabel = UILabel()
    internal var imageView = UIImageView()
    var title: String? {
        get {
            titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }
    var subtitle: String? {
        get {
            subtitleLabel.text
        }
        set {
            subtitleLabel.text = newValue
        }
    }
    var image: UIImage? {
        get {
            imageView.image
        }
        set {
            imageView.image = newValue
        }
    }
    
}

#endif

final class CastCell: UICollectionViewCell {
    
    @IBOutlet var monogramView: TVMonogramView!
    
    private var imageView: UIImageView?
    
    var cast:Cast? {
        didSet {
            if let cast = self.cast,
                self.monogramView != nil {
                self.monogramView.title = cast.role
                self.monogramView.subtitle = cast.name
                if let image = cast.thumbnail,
                    !image.isEmpty,
                    let imageUrl = URL(string: image){
                    self.imageView = UIImageView(frame: self.monogramView.frame)
                    self.imageView?.setCashedImage(url: imageUrl, type: .emptyLoading,
                                                   completionHandler: { [weak self] result in
                                                    guard let self = self else { return }
                        switch result {
                        case .success(let data):
                            self.monogramView.image = data.image
                        case .failure(_):
                            break
                        }
                    })
                } else {
                    self.monogramView.image = nil
                }
#if canImport(TVUIKit)
                var component = PersonNameComponents()
                let nameArr:[String]? = cast.name?.split(separator: " ").map { String($0) }
                if (nameArr?.count ?? 0) >= 2 {
                    component.givenName = nameArr?.first
                    component.familyName = nameArr?.last
                }
                
                self.monogramView.personNameComponents = component
                #endif
            }
        }
    }
}
