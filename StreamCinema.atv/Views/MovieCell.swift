//
//  MovieCell.swift
//  MovieDBTV
//
//  Created by Alfian Losari on 23/03/19.
//  Copyright © 2019 Alfian Losari. All rights reserved.
//

import UIKit
import Kingfisher

final class MovieCell: UICollectionViewCell {
    
    @IBOutlet var watchedImage: UIImageView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet weak var audioLangLabel: UILabel!
    
    @IBOutlet weak var typeLabelUnfocusedC: NSLayoutConstraint!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet var unfocusedConstraint: NSLayoutConstraint!
    @IBOutlet weak var watchedStateUnfocusedC: NSLayoutConstraint!

    var focusedConstraints: [NSLayoutConstraint] = []
    var unFocusedConstraints: [NSLayoutConstraint] = []
    
    public var data:SCCMovie?
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        
        return formatter
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        #if canImport(TVUIKit)
        focusedConstraints.removeAll()
        focusedConstraints.append(audioLangLabel.topAnchor.constraint(equalTo: imageView.focusedFrameGuide.bottomAnchor,
                                                                      constant: 16))
        focusedConstraints.append(typeLabel.topAnchor.constraint(equalTo: imageView.focusedFrameGuide.topAnchor,
                                                                      constant: 16))
        focusedConstraints.append(watchedImage.topAnchor.constraint(equalTo: imageView.focusedFrameGuide.topAnchor,
                                                                      constant: 16))
        unFocusedConstraints.removeAll()
        unFocusedConstraints.append(unfocusedConstraint)
        unFocusedConstraints.append(typeLabelUnfocusedC)
        unFocusedConstraints.append(watchedStateUnfocusedC)
        #endif
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        focusedConstraints.forEach({ $0.isActive = isFocused })
        unFocusedConstraints.forEach({ $0.isActive = !isFocused })
        
        if let label = titleLabel as? MarqueeLabel {
            label.type = .leftRight
            if isFocused {
                label.holdScrolling = false
            } else if label.holdScrolling == false {
                label.holdScrolling = true
                label.shutdownLabel()
            }
        }
    }
    
    private func isWatched(_ watchedState: WatchedState) {
        self.watchedImage.isHidden = watchedState == .none
        self.watchedImage.layer.shadowColor = UIColor.darkGray.cgColor
        self.watchedImage.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.watchedImage.layer.shadowOpacity = 1
        self.watchedImage.layer.shadowRadius = 1.0
        self.watchedImage.clipsToBounds = false
        
        var image: UIImage? = nil
        if watchedState == .done {
            image = UIImage(systemName: "checkmark.circle.fill")
        } else if watchedState == .paused {
            image = UIImage(systemName: "pause.circle.fill")
        } else if watchedState == .trakt {
            image = UIImage(named: "traktLogo")
        }
        let config = UIImage.SymbolConfiguration(paletteColors: [.white, .systemMint])
        
        self.watchedImage.image = image?.applyingSymbolConfiguration(config)
    }
    
    func configure(_ movie: SCCMovie) {
        self.data = movie
        imageView.kf.indicatorType = .none
        if let url = movie.poster {
            self.imageView.setCashedImage(url: url, type: .poster)
        } else {
            self.imageView.image = UIImage(named: "poster")
        }
        
        ratingLabel.textColor = .secondaryLabel
        if let rating = movie.rating {
            ratingLabel.text = rating.ratingTitle()
            ratingLabel.isHidden = false
        } else {
            ratingLabel.isHidden = true
        }
        
        if let streams = movie.auiodLangs {
            let audioString = NSMutableAttributedString(string: streams.joined(separator: ", "))
            let range = NSRange(location: 0, length: audioString.length)
            audioString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
            audioLangLabel.attributedText = audioString
            audioLangLabel.isEnabled = false
        } else {
            audioLangLabel.isEnabled = true
        }
        
        let string:NSMutableAttributedString = NSMutableAttributedString(string: movie.title + " ")
        titleLabel.attributedText = string
        if let state = self.data?.getCurrentState() {
            self.isWatched(state)
        } else {
            self.isWatched(.none)
        }
        
        typeLabel.clipsToBounds = true
        typeLabel.layer.cornerRadius = 8
        
        if movie.traktType == .movie {
            typeLabel.isHidden = false
            typeLabel.text = "Movie"
        } else if movie.traktType == .tvshow {
            typeLabel.isHidden = false
            typeLabel.text = "Show"
        } else {
            typeLabel.isHidden = true
        }
    }
    
    public func setWatchedFromTrakt() {
        self.isWatched(.trakt)
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        
        setNeedsUpdateConstraints()
        coordinator.addCoordinatedAnimations({
            self.layoutIfNeeded()
        }, completion: nil)
    }    
}
