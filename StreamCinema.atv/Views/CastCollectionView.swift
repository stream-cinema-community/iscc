//
//  CastCollectionView.swift
//  StreamCinema.atv
//
//  Created by SCC on 21/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//
#if canImport(TVUIKit)
import TVUIKit
#endif
import UIKit



protocol CastCollectionViewDelegate: AnyObject {
    func castCollectionView(_ castView:CastCollectionView, didSelect cast:Cast)
}

final class CastCollectionView: UICollectionView {
    public weak var castDelegate:CastCollectionViewDelegate?
    var cast:[Cast] = [] {
        didSet {
            self.reloadData()
        }
    }
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.setupCollectionView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupCollectionView()
    }
    
    private func setupCollectionView() {
        self.delegate = self
        self.dataSource = self
        self.register(UINib(nibName: "CastCell", bundle: nil), forCellWithReuseIdentifier: "CastCell")
    }
    
    public func configureBlur() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.backgroundView = blurEffectView
    }
}


extension CastCollectionView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CastCell", for: indexPath) as? CastCell else { return UICollectionViewCell() }
        let cast = self.cast[indexPath.row]
        cell.cast = cast
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cast = self.cast[indexPath.row]
        self.castDelegate?.castCollectionView(self, didSelect: cast)
    }
}
