//
//  TVShowEpisodeAndSeasonSelector.swift
//  StreamCinema.atv
//
//  Created by iSCC on 27/02/2023.
//  Copyright © 2023 SCC. All rights reserved.
//

import Foundation
import ParallaxView

final class TVShowEpisodeAndSeasonSelectorModel {
    var appData: AppData
    var currentShow: SCCMovie
    var seasons: [SCCMovie] = []
    var episodes: [SCCMovie] = []
    var selectedSeasonIndex: Int
    var selectedEpisodeIndex: Int
    var onUpdateEpisodes:([SCCMovie])->Void
    var onUpdateSelect: (Selected) -> Void
    var isPossibleFetchNextEpisodesData: Bool = true
    
    var onUpdateData:(() -> Void)?
    var onError:((Error) -> Void)?
    
    var selectedSeason: SCCMovie? {
        seasons[safe: selectedSeasonIndex]
    }
    
    init(appData: AppData,
         currentShow: SCCMovie,
         seasons: [SCCMovie],
         episodes: [SCCMovie],
         selectedSeasonIndex: Int,
         selectedEpisodeIndex: Int,
         onUpdateEpisodes:@escaping ([SCCMovie]) -> Void,
         onUpdateSelect: @escaping (Selected) -> Void)
    {
        self.appData = appData
        self.currentShow = currentShow
        self.seasons = seasons
        self.episodes = episodes
        self.selectedSeasonIndex = selectedSeasonIndex
        self.selectedEpisodeIndex = selectedEpisodeIndex
        self.onUpdateEpisodes = onUpdateEpisodes
        self.onUpdateSelect = onUpdateSelect
    }
    
    func updateShowData(is selected: Selected, episodes: [SCCMovie], needAppedn: Bool) {
        if needAppedn {
            self.episodes.append(contentsOf: episodes)
        } else {
            self.episodes = episodes
        }
        
        selectedSeasonIndex = seasons.firstIndex(where: { $0.season == selected.season }) ?? selectedSeasonIndex
        selectedEpisodeIndex = episodes.firstIndex(where: { $0.episode == selected.episode }) ?? selectedEpisodeIndex
        onUpdateData?()
    }
    
    func fetchEpisodesFor(_ season: SCCMovie, atIndex: Int) {
        guard let seasionSccId = season.seasonIDs?.sccID else { return }
        Task {
            do {
                guard let episodesF = try await appData.scService.getEpisodes(for: seasionSccId,
                                                                             showID: currentShow.ids,
                                                                             seasonID: season.ids,
                                                                             startIndex: 0, offSet: 100)?.data
                else
                { return }
                self.episodes = episodesF
                selectedSeasonIndex = atIndex
                selectedEpisodeIndex = 0
                onUpdateEpisodes(episodesF)
                onUpdateSelect(Selected(season: season.season ?? atIndex , episode: selectedEpisodeIndex))
                onUpdateData?()
                isPossibleFetchNextEpisodesData = episodesF.count == 100
            } catch {
                onError?(error)
            }
        }
    }
    
    func fetchNextEpisodes() {
        guard isPossibleFetchNextEpisodesData,
              let selectedSeason,
              let seasionSccId = selectedSeason.seasonIDs?.sccID
        else { return }
        
        Task {
            do {
                guard let episodesF = try await appData.scService.getEpisodes(for: seasionSccId,
                                                                             showID: currentShow.ids,
                                                                             seasonID: selectedSeason.ids,
                                                                             startIndex: self.episodes.count,
                                                                             offSet: 100)?.data
                else
                { return }
                self.episodes.append(contentsOf: episodesF)
                onUpdateEpisodes(self.episodes)
                onUpdateData?()
                isPossibleFetchNextEpisodesData = episodesF.count == 100
            } catch {
                onError?(error)
            }
        }
    }
}

protocol TVShowEpisodeAndSeasonSelectorDelegate: AnyObject {
    func selector(_ cell:TVShowEpisodeAndSeasonSelectorCell, didSelect movie: SCCMovie)
    func selector(_ cell:TVShowEpisodeAndSeasonSelectorCell, nextPage forSeason: SCCMovie?)
}

final class TVShowEpisodeAndSeasonSelectorCell: UITableViewCell {
    @IBOutlet weak var seasonButton: UIButton!
    @IBOutlet weak var seasonTopInfo: UILabel!
    @IBOutlet weak var episodeDesc: UITextView!
    @IBOutlet weak var episodesCollecitionView: UICollectionView!
    @IBOutlet weak var buttonGoToEnd: UIButton!
    
    weak var delegate: TVShowEpisodeAndSeasonSelectorDelegate?
    weak var parentController: UIViewController?
    
    var model:TVShowEpisodeAndSeasonSelectorModel? {
        didSet {
            configureViews()
            
            model?.onUpdateData = { [weak self] in
                DispatchQueue.main.async {
                    self?.episodesCollecitionView.reloadData()
                    self?.configureViews()
                }
            }
        }
    }
    
    internal func configureViews() {
        if episodesCollecitionView.delegate == nil {
            episodesCollecitionView.delegate = self
            episodesCollecitionView.dataSource = self
            episodesCollecitionView.remembersLastFocusedIndexPath = true
            episodesCollecitionView.register(MyEpisodeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        }
        guard let model else { return }
        
        if let season = model.seasons[safe: model.selectedSeasonIndex] ?? model.seasons.first {
            let title = String(localized: .season) + ": \(season.season ?? 1)"
            seasonButton.setTitle(title, for: .normal)
            
            let attributedText = NSMutableAttributedString(string: "")
            if let movieData = season.movieData {
                for item in movieData {
                    let string = formatedString(for: item.keys.first, value: item.values.first)
                    attributedText.append(string)
                    if movieData.last != item {
                        attributedText.append(NSMutableAttributedString(string: "  "))
                    } else if let rating = season.rating {
                        attributedText.append(NSMutableAttributedString(string: "\n"))
                        attributedText.append(rating.allRatings())
                    }
                }
            }
            
            seasonTopInfo.attributedText = attributedText
            seasonButton.isHidden = false
        } else {
            seasonButton.isHidden = true
        }
        
        if model.episodes.count > 99 {
            buttonGoToEnd.isHidden = false
        } else {
            buttonGoToEnd.isHidden = true
        }
        
        updateData(at: IndexPath(row: model.selectedEpisodeIndex, section: 0))
    }
    
    internal func updateData(at indexPath: IndexPath) {
        if let model = model?.episodes[safe: indexPath.row] {
            var titleString = ""
            titleString = model.title + "\n"
            
            if let episodeNumber = model.episode, "\(episodeNumber)" != model.title {
                titleString = "\(episodeNumber) - " + model.title
            }
            
            if let langs = model.auiodLangs?.joined(separator: ", ") {
                titleString += "\n"
                titleString += "Audio: \(langs)"
            }
            titleString += "\n"
            
            let title = NSMutableAttributedString(string: "\(titleString)",
                                                attributes: [.foregroundColor:UIColor.label,
                                                             NSAttributedString.Key.font:UIFont.systemFont(ofSize: 20, weight: .medium)])
            
            let desc = NSMutableAttributedString(string: "\(model.desc ?? "")",
                                                attributes: [.foregroundColor:UIColor.label,
                                                             NSAttributedString.Key.font:UIFont.systemFont(ofSize: 20, weight: .regular)])
            title.append(desc)
            
            episodeDesc.attributedText = title
            episodeDesc.layoutIfNeeded()
            
            episodeDesc.contentInsetAdjustmentBehavior = .always
        }
    }
    
    internal func formatedString(for key:String?, value: String?) -> NSAttributedString {
        let keyStr = NSMutableAttributedString(string: "\(key ?? ""): ",
                                            attributes: [.foregroundColor:UIColor.label,
                                                         NSAttributedString.Key.font:UIFont.systemFont(ofSize: 20, weight: .medium)])
        let valueStr = NSMutableAttributedString(string: "\(value ?? "")",
                                            attributes: [.foregroundColor:UIColor.label,
                                                         NSAttributedString.Key.font:UIFont.systemFont(ofSize: 17)])
        keyStr.append(valueStr)
        return keyStr
    }
}

// MARK: - collectionView
extension TVShowEpisodeAndSeasonSelectorCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        model?.episodes.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyEpisodeCollectionViewCell
        
        if var model = model?.episodes[safe: indexPath.row] {
            let imageURL = model.fanart ?? model.banner ?? model.thumb
            cell.imageUrl = imageURL
            
            if imageURL == nil {
                Task {
                    if let imageUrl = await model.episodeBackdrop() {
                        cell.imageUrl = imageUrl
                    }
                }
            }
            
            if model.progress == nil {
                model.getCurrentState()
            }
            if model.state == .done {
                cell.progress = 100
            } else {
                cell.progress = model.progress
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didUpdateFocusIn context: UICollectionViewFocusUpdateContext,
                        with coordinator: UIFocusAnimationCoordinator) {
        
        if let focusedIndexPath = context.nextFocusedIndexPath {
            updateData(at: focusedIndexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/4
        let height = collectionView.frame.size.height - 40
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath)
    {
        if let model,
           model.episodes.count != 0,
           (model.episodes.count - 1) == indexPath.row,
            indexPath.row < model.episodes.count {
            let lastIndex = model.episodes[indexPath.row].totalCount - 1
            if lastIndex <= indexPath.row {
                return
            }
            model.fetchNextEpisodes()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let episode = model?.episodes[safe: indexPath.row] else { return }
        delegate?.selector(self, didSelect: episode)
    }
    
    func indexPathForPreferredFocusedView(in collectionView: UICollectionView) -> IndexPath? {
        guard let selectedEpisodeIndex = model?.selectedEpisodeIndex else { return nil }
        return IndexPath(row: selectedEpisodeIndex, section: 0)
    }
}

// MARK: - Actions
extension TVShowEpisodeAndSeasonSelectorCell {
    @IBAction func seasonButtonPressed() {
        guard let model else { return }
        let alert = UIAlertController(title: String(localized: .season), message: nil, preferredStyle: .actionSheet)
        
        for (index, season) in model.seasons.enumerated() {
            let action = UIAlertAction(title: season.title, style: .default) { _ in
                model.fetchEpisodesFor(season, atIndex: index)
            }
            alert.addAction(action)
            
            if index == model.selectedSeasonIndex {
                action.setValue(true, forKey: "checked")
                alert.preferredAction = action
            }
        }
        
        alert.popoverPresentationController?.sourceView = seasonButton
        alert.addAction(UIAlertAction(title: String(localized: .buttonCancel), style: .cancel))
        parentController?.present(alert, animated: true)
    }
    
    @IBAction func goToEndButtonPressed() {
        guard let model else { return }
        let lastEpisodeIndex = model.episodes.count - 1
        let indexPath = IndexPath(row: lastEpisodeIndex, section: 0)
        episodesCollecitionView.scrollToItem(at: indexPath,
                                             at: .right,
                                             animated: true)
        model.selectedEpisodeIndex = lastEpisodeIndex
        updateData(at: indexPath)
    }
}


final class MyEpisodeCollectionViewCell: ParallaxCollectionViewCell {
    let progressBar = UIProgressView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureViews()
    }
    
    var imageUrl: URL? {
        didSet {
            if let imageUrl = self.imageUrl {
                (backgroundView as? UIImageView)?.setCashedImage(url: imageUrl, type: .fanart)
            } else {
                (backgroundView as? UIImageView)?.image = UIImage(named: "episode")
            }
        }
    }
    
    var progress: Double? {
        didSet {
            if let progress {
                progressBar.isHidden = false
                progressBar.progress = Float(progress/100)
            } else {
                progressBar.isHidden = true
            }
        }
    }
    
    internal func configureViews() {
        addSubview(progressBar)
        
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        
        progressBar.heightAnchor.constraint(equalToConstant: 8).isActive = true
        progressBar.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        progressBar.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        progressBar.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        if backgroundView == nil {
            let image = UIImage(named: "episode")
            backgroundView = UIImageView(image: image)
        }
    }
}
