//
//  Cast.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine

final class CastCellView: UICollectionViewCell {
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    private var cancelables: Set<AnyCancellable> = Set()
    
    var staffModel: MovieStaff? {
        didSet {
            movieNameLabel.text = staffModel?.name
            nameLabel.text = staffModel?.type.title
            
            personImage.backgroundColor = .clear
            
            if staffModel?.type == .studios {
                if let imageUrl = staffModel?.image {
                    personImage.kf.setImage(with: imageUrl)
                    personImage.backgroundColor = .white.withAlphaComponent(0.2)
                } else {
                    loadImage()
                }
            }
        }
    }
    
    internal func loadImage() {
        staffModel?.findImageForCurrentResult()
            .sink(receiveCompletion: { err in
                print(err)
            }, receiveValue: { [weak self] url in
                if let url {
                    self?.personImage?.kf.setImage(with: url)
                    self?.personImage.backgroundColor = .white.withAlphaComponent(0.2)
                }
            })
            .store(in: &cancelables)
    }

    
    public func configure() {
        self.personImage.clipsToBounds = true
        self.personImage.image = UIImage(systemName: "person.circle.fill")
        self.personImage.layer.cornerRadius = self.personImage.frame.size.height/2
        self.clipsToBounds = true
        self.layer.cornerRadius = 12
    }
}
