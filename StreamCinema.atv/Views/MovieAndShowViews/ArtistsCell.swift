//
//  ArtistsCell.swift
//  StreamCinema.atv
//
//  Created by SCC on 14/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

protocol ArtistsCellDelegate: AnyObject {
    func artistsCell(_ artistsCell:ArtistsCell, didSelect artist:Cast)
    func artistsCell(_ artistsCell:ArtistsCell, didSelect staff:MovieStaff)
}

final class ArtistsCell: UITableViewCell, MovieDetailCellProtocol {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistColletionView: UICollectionView!
    
    weak var delegate: ArtistsCellDelegate?
    var type: MovieViewCells?
    var model: [Cast] = [] {
        didSet {
            self.configureView()
            self.artistColletionView?.reloadData()
        }
    }
    
    var staff: [MovieStaff] = [] {
        didSet {
            self.configureView()
            self.artistColletionView?.reloadData()
        }
    }
    
    func configureView() {
        self.titleLabel.text = String(localized: .artist)
        if type == .movieStaff {
            self.titleLabel.text = String(localized: .staff)
        }
        
        guard self.artistColletionView.delegate == nil else { return }
        self.artistColletionView.delegate = self
        self.self.artistColletionView.dataSource = self
        self.artistColletionView.register(UINib(nibName: "CastCellView", bundle: nil), forCellWithReuseIdentifier: "CastCellView")
        self.artistColletionView.remembersLastFocusedIndexPath = true
    }
    
}

extension ArtistsCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if type == .movieStaff   {
            return staff.count
        }
        return self.model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier:"CastCellView", for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? CastCellView else { return }
        
        if type == .movieStaff  {
            guard staff.count > indexPath.row else { return }
            let currentStaff = staff[indexPath.row]
            cell.staffModel = currentStaff
            return
        }
        
        cell.configure()
        
        if self.model.count > indexPath.row {
            let data = self.model[indexPath.row]
            cell.movieNameLabel.text = data.role
            cell.nameLabel.text = data.name
            if let string = data.thumbnail, let url = URL(string: string) {
                cell.personImage.setCashedImage(url: url, type: .emptyLoading)
            }
            cell.personImage.backgroundColor = .clear
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if type == .movieStaff   {
            guard staff.count > indexPath.row else { return }
            let currentStaff = staff[indexPath.row]
            self.delegate?.artistsCell(self, didSelect: currentStaff)
            return
        }
        guard self.model.count > indexPath.row else { return }
        self.delegate?.artistsCell(self, didSelect: self.model[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        
        if let indexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: indexPath) as? CastCellView {
            cell.backgroundColor = .none
        }
        if let nextIndexPath = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: nextIndexPath) as? CastCellView {
            cell.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        }
    }
}
