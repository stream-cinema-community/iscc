//
//  UIImage+.swift
//  StreamCinema.atv
//
//  Created by SCC on 30/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Kingfisher
import QuartzCore

enum UIImageViewCashedType {
    case banner
    case poster
    case fanart
    case emptyLoading
}

extension UIImageView {
    func setCashedImage(url:URL, type:UIImageViewCashedType, completionHandler: ((Result<RetrieveImageResult, KingfisherError>) -> Void)? = nil) {
        var image:UIImage?
        if type == .banner {
            image = UIImage(named: "banner")
        } else if type == .poster {
            image = UIImage(named: "poster")
        } else if type == .fanart {
            image = UIImage(named: "fanart")
        }
        let processor = DownsamplingImageProcessor(size: self.bounds.size)
        self.kf.indicatorType = .none
        self.kf.setImage(
            with: url.standardized,
            placeholder: image,
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5))
            ],
            completionHandler:
                {
                    result in
                    if let completionHandler = completionHandler {
                        completionHandler(result)
                    }
                    switch result {
                    case .success(let value):
                        Log.write("setCashedImage Task done for: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        Log.write("setCashedImage Job failed: \(error.localizedDescription)")
                    }
                })
    }
    
    func setCashedImageS(url:URL, type:UIImageViewCashedType, completionHandler: ((Bool) -> Void)? = nil) {
        var image:UIImage?
        if type == .banner {
            image = UIImage(named: "banner")
        } else if type == .poster {
            image = UIImage(named: "poster")
        } else if type == .poster {
            image = UIImage(named: "fanart")
        }
        self.kf.indicatorType = .none
        self.kf.setImage(
            with: url,
            placeholder: image,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5))
            ],
            completionHandler:
                {
                    result in
                    switch result {
                    case .success(let value):
                        completionHandler?(true)
                        Log.write("setCashedImageS Task done for: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        completionHandler?(false)
                        Log.write("setCashedImageS Job failed: \(error.localizedDescription)")
                    }
                })
    }
}
