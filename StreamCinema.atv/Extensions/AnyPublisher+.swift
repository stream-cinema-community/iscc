//
//  AnyPublisher+.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 27.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Combine
import UIKit

extension AnyPublisher where Failure == Error {


    func assignError(to subject: PassthroughSubject<Error, Never>) -> AnyPublisher<Output, Never> {

        self
            .catch { (error) -> AnyPublisher<Output, Never> in
                subject.send(error)
                return Empty<Output, Never>()
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}


