//
//  Bundle+.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

extension UIStoryboard {

    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: .main)
    }

}
