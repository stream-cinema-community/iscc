//
//  ActorMoviesViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine
import TMDBKit
import Kingfisher

final class ActorMoviesViewController: UIViewController {

    static func create(appData: AppData) -> ActorMoviesViewController {
        let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "ActorMoviesViewController") as! ActorMoviesViewController
        vc.appData = appData
        return vc
    }
    
    @IBOutlet weak var actorImageView: UIImageView!
    @IBOutlet weak var actorNameLabel: UILabel!
    @IBOutlet weak var movieCollection: MovieCollectionView!
    @IBOutlet weak var activityIndikator: UIActivityIndicatorView!
    private var coordinator: MovieCoordinator?
    private var cast:Cast?
    private var staff: MovieStaff?
    private var movieTitle:String?

    private var appData: AppData!
    private var cancelables: Set<AnyCancellable> = Set()
    private let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()

    var onPresentMovieDetailScreen: ((SCCMovie) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.movieCollection.movieDelegate = self
        
        
        if staff != nil {
            actorImageView.isHidden = true
            self.fetchData()
        }
        else if let cast = self.cast {
            if let thumbnail = cast.thumbnail,
                let actorImgUrl = URL(string: thumbnail) {
                self.actorImageView.setCashedImage(url: actorImgUrl, type: .emptyLoading)
            }
            self.fetchData()
        } else if let model = self.movieCollection.model {
            self.movieCollection.model = model
        }
        
        self.actorNameLabel.attributedText = name
        
        loadImageForStaffIfNeeded()
    }
    
    var name: NSAttributedString {
        get {
            var name: NSMutableAttributedString
            if let staff {
                name = NSMutableAttributedString(string: staff.name)
            }
            else if let castName = cast?.name {
                name = NSMutableAttributedString(string: castName)
            }
            else if let title = self.movieTitle {
                name = NSMutableAttributedString(string: title)
            }
            else {
                name = NSMutableAttributedString(string: "None")
            }
            let count = NSAttributedString(string: " (\(self.movieCollection.model?.data.count ?? 0)/\(self.movieCollection.model?.totalCount ?? 0))", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20)])
            name.append(count)
            
            return name
        }
    }
    
    public func prepareData(for staff: MovieStaff) {
        self.staff = staff
    }
    
    internal func loadImageForStaffIfNeeded() {
        if staff?.type == .studios {
            if let imageUrl = staff?.image {
                self.loadImage(from: imageUrl)
                self.actorImageView.isHidden = false
            } else {
                staff?.findImageForCurrentResult()
                    .sink(receiveCompletion: { err in
                        print(err)
                    }, receiveValue: { [weak self] url in
                        if let url {
                            self?.loadImage(from: url)
                            self?.actorImageView.isHidden = false
                        }
                    })
                    .store(in: &cancelables)
            }
        }
    }
    
    internal func loadImage(from url: URL) {
        actorImageView.kf.indicatorType = .none
        actorImageView.kf.setImage(
            with: url,
            placeholder: nil,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5))
            ],
            completionHandler: { [weak self] _ in
                    guard let self else { return }
                    guard let image = self.actorImageView.image else { return }
                    if let model = image.cgImage?.colorSpace?.model,
                       model == .monochrome {
                        self.actorImageView.image = image.withRenderingMode(.alwaysTemplate)
                    }
                }
        )
    }
    
    public func prepareData(with cast:Cast) {
        self.cast = cast
    }
    
    public func prepareData(with model:SCCMovieResult, releated title:String) {
        self.movieTitle = title
        self.movieCollection.model = model
    }
    
    private func fetchData(for startIndex:Int = 0) {
        guard activityIndikator.isAnimating == false else { return }
        self.activityIndikator.startAnimating()
        
        var filter: CustomFilterModel?
        if let castName = cast?.name { // CAST
            filter = CustomFilterModel(with: castName, startIndex: startIndex)
        } else if let staff {
            filter = CustomFilterModel(with: staff, startIndex: startIndex)
        }
        
        guard let filter else { return }
        appData.scService
            .customSearch(wtih: filter)
            .sink { [weak self] completion in
                guard case .failure(let err) = completion else { return }
                self?.activityIndikator.stopAnimating()
                self?.errorSubject.send(err)
            } receiveValue: { [weak self] (filter) in
                guard let self else { return }
                self.activityIndikator.stopAnimating()
                if self.movieCollection.model == nil {
                    var filterResult = filter
                    filterResult.sortByDateCreate()
                    self.movieCollection.model = filterResult
                } else {
                    self.movieCollection.model?.data.append(contentsOf: filter.data)
                    if self.movieCollection.model?.data.count == filter.totalCount {
                        self.sortAlertShow()
                    }
                }
                self.actorNameLabel.attributedText = self.name
            }.store(in: &cancelables)
    }
    
    func sortAlertShow() {
        
        let alert = UIAlertController(title: String(localized: .sortAlertTitle),
                                      message: String(localized: .sortAlertText),
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: String(localized: .button_sort), style: .default, handler: { _ in
            self.movieCollection.model?.sortByDateCreate()
            self.movieCollection.reloadData()
        }))
        alert.addAction(UIAlertAction(title: String(localized: .buttonCancel), style: .cancel))
        present(alert, animated: true)
    }
}

extension ActorMoviesViewController: MovieCollectionDelegate {
    func movieCollection(_ collection: MovieCollectionView, didSelect movie: SCCMovie) {
        onPresentMovieDetailScreen?(movie)
    }
    
    func movieCollection(_ collection: MovieCollectionView, startIndex: Int) {
        if self.cast != nil || staff != nil {
            self.fetchData(for: startIndex)
        }
    }
}
