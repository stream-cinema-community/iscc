////
////  FFMPEGPlayerViewController.swift
////  StreamCinema.atv
////
////  Created by Martin Matějka on 03.02.2021.
////  Copyright © 2021 SCC. All rights reserved.
////
//
//import UIKit
//import mobileffmpeg
//import AVKit
//import AVFoundation
//
//class FFMPEGPlayerViewController: UIViewController {
//
//    private let urlString = "https://vip.3.dl.webshare.cz/7152/5kh7o1Qa4x/524288000/eJw1j11LwzAYhf9KeC+8Kmnz2SQwZAyEIVS8EEEKI2kyJtuakXabVPzvJoKX5+HhcM43WDDQKswF1gxzDRXMYIgkjHIlKKnglmMFVzBcE1rA9AcuYOZ0DRWMueApxSWMaLtFpFHNBa03DAlM0OYDn4+33OmztOeKikZx61jraMucDEoPfGCOcq333jMhmeCu6HPxP09h5+N9PEXrM0yZ3YObDjYFPCx9XYS+FsdDG8mr5V99_RhSimn11j13L+_dQ1yVO+l_6bSA0Q0hUsl87ecXsH1ExQ/94b6e60cc55bd7ad15ceb4844729637f035da408/Frozen-II-1080p-AC3-5.1-CZ.mkv"
//
//    private let urlString2 = Bundle.main.path(forResource: "frozen-ii-710623", ofType: "mkv")
//
//    private let urlString3 = "http://itsvet.net/KODI/ATV_SCC/exaples/Dolby_Unfold_Lossless_ATMOS-thedigitaltheater.mkv"
//
//    private var movieLocation: URL?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//        view.backgroundColor = UIColor.yellow
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//
//        self.setupVideo()
//
//
//    }
//
//
//    private func setupVideo() {
//        let temp = FileManager.default.temporaryDirectory
//        let outputFilePath = temp.absoluteString + UUID().uuidString + ".mp4"
//
//        guard let url = URL(string: outputFilePath) else { return }
//        print("[FFMPEG] URL of file is: \(url)")
//        self.movieLocation = url
//
//        MobileFFmpegConfig.setLogDelegate(self)
////        guard let file = Bundle.main.path(forResource: "frozen-ii-710623", ofType: "mkv") else {
////            print("[FFMPEG] file not found")
////            return
////        }
//
//        MobileFFmpeg.executeAsync("-i \(urlString3) -c:v libx265 -c:a copy -flags +global_header \(outputFilePath)", withCallback: self)
////        MobileFFmpeg.executeAsync("-protocols", withCallback: self)
//
//    }
//
//    private func printMediaInfo() {
//        guard let info = MobileFFprobe.getMediaInformation(urlString) else {
//            print("[FFMPEG] NO INFO")
//            return
//        }
//        print("[FFMPEG] Media info formate: \(info.getFormat() ?? "unknown")")
//        print("[FFMPEG] Streams: \(info.getStreams() ?? [])")
//    }
//
//
//   func playVideo() {
//        guard let url = self.movieLocation else {
//            print("[FFMPEG] NO URL")
//            return
//        }
//        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
//        let player = AVPlayer(url: url)
//
//        // Create a new AVPlayerViewController and pass it a reference to the player.
//        let controller = AVPlayerViewController()
//        controller.player = player
//
//        // Modally present the player and call the player's play() method when complete.
//        present(controller, animated: true) {
//            player.play()
//        }
//    }
//
//
//
//}
//
//extension FFMPEGPlayerViewController: LogDelegate {
//    func logCallback(_ executionId: Int, _ level: Int32, _ message: String!) {
//        print("[FFMPEG] Message: ", message!)
//    }
//
//
//}
//
//extension FFMPEGPlayerViewController: ExecuteDelegate {
//
//    func executeCallback(_ executionId: Int, _ returnCode: Int32) {
//        if (returnCode == RETURN_CODE_SUCCESS) {
//            print("[FFMPEG] Async command execution completed successfully.\n")
//            DispatchQueue.main.async {
//                self.playVideo()
//            }
//        } else if (returnCode == RETURN_CODE_CANCEL) {
//            print("[FFMPEG] Async command execution cancelled by user.\n")
//        } else {
//            print("[FFMPEG] Async command execution failed with rc=%d.\n", returnCode)
//        }
//    }
//
//
//}
