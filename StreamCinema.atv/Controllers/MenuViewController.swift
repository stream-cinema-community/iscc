//
//  MenuViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 28/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

protocol MenuViewDelegate {
    var type: FilterType? { get set }
    func menuView(_ menuView: MenuViewController, did select:MenuItem, for type:FilterType?)
    func menuView(_ menuView: MenuViewController, settingsDid select:SettingsMenuItem)
}

final class MenuViewController: UITableViewController {
    private var items:[MenuItem] = [.friendsAdvise, .popular, .trending, .news, .mostWatched, .dubbed, .lastAdded, .genre, .christmas, .watched, .watchedList]
    private let settingsItems:[SettingsMenuItem] = [.webshare, .trakt, .openSubtitles, .subtitles, .friends]
    private let traktItems:[MenuItem] = [.traktHistory,.traktWatchList]
    private var selectedItem: IndexPath = IndexPath(row: 0, section: 0)
    
    public var endpoit:TabBarItem? {
        willSet {
            self.type = newValue?.type
            if let type {
                if type == .concert {
                    self.items = [.popular, .trending, .news, .mostWatched, .dubbed, .lastAdded, .watched]
                }
                else if type != .movie {
                    self.items.removeAll(where: { $0 == .christmas })
                }
                if FriendData.friends.count == 0 {
                    self.items.removeAll(where: { $0 == .friendsAdvise })
                }
            }
        }
    }
    public var type:FilterType?
    public var menuDelegate:MenuViewDelegate? {
        didSet {
            self.menuDelegate?.type = self.endpoit?.type
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.remembersLastFocusedIndexPath = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.selectRow(at: self.selectedItem, animated: false, scrollPosition: .middle)
        if let endpoint = self.endpoit {
            if endpoint == .settings {
                
            } else if endpoint == .trakt {
                self.menuDelegate?.menuView(self, did: self.traktItems[self.selectedItem.row], for: type)
            } else {
                self.menuDelegate?.menuView(self, did: self.items[self.selectedItem.row], for: type)
            }
        }
    }
    
    func insert(menu item: MenuItem) {
        guard let type, type == .movie || type == .tvshow else { return }
        if !items.contains(where: { $0 == item }) {
            items.insert(item, at: 0)
            tableView.reloadData()
        }
    }
}

extension MenuViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        if self.endpoit != nil {
            return 1
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let endpoint = self.endpoit {
            if endpoint == .settings {
                return settingsItems.count
            } else if endpoint == .trakt {
                return traktItems.count
            } else {
                return items.count
            }
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        
        if let endpoint = self.endpoit {
            if endpoint == .settings {
                cell.textLabel?.text = self.settingsItems[indexPath.row].description
            } else if endpoint == .trakt {
                cell.textLabel?.text = self.traktItems[indexPath.row].description
            } else {
                cell.textLabel?.text = self.items[indexPath.row].description
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let indexPath = context.nextFocusedIndexPath,
            self.type == nil {
            self.menuDelegate?.menuView(self, settingsDid: self.settingsItems[indexPath.row])
            self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let type = self.type {
            if type == .trakt {
                self.menuDelegate?.menuView(self, did: self.traktItems[indexPath.row], for: type)
                return
            }
            self.menuDelegate?.menuView(self, did: self.items[indexPath.row], for: type)
        } else {
        #if os(iOS)
            self.menuDelegate?.menuView(self, settingsDid: self.settingsItems[indexPath.row])
            self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        #endif
        }
        self.selectedItem = indexPath
    }
}

