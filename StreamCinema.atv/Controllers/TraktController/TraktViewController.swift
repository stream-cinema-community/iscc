//
//  TraktViewController.swift
//  StreamCinema.atv
//
//  Created by iSCC on 16/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import Combine

protocol TraktViewControllerDelegate: AnyObject {
    func traktView(_ controller: TraktViewController, didSelect item: SCCMovie)
}

final class TraktViewController: UITableViewController {

    static func create(viewModel: TraktViewModel) -> TraktViewController {
        let board = UIStoryboard(name: "TraktStoryboard", bundle: nil)
        let vc = board.instantiateViewController(identifier: "TraktViewController") as! TraktViewController
        vc.title = TabBarItem.trakt.description
        vc.tabBarItem.image = UIImage(named: "trakt")
        vc.viewModel = viewModel
        return vc
    }

    private(set) var viewModel: TraktViewModel!
    public weak var movieDelegate:TraktViewControllerDelegate?
    private var cancelables: Set<AnyCancellable> = Set()
    
    public func set(_ viewModel: TraktViewModel) {
        self.viewModel = viewModel
        self.tableView.reloadData()
        self.tableView.remembersLastFocusedIndexPath = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        setupMenuButtonToForceFocusOnTabBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    private func setupViewModel() {
        viewModel.errorSubject
            .sink { [weak self] (error) in
                guard let otherErr = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherErr)
            }.store(in: &cancelables)

        viewModel.onReloadData = { indexPath in
            DispatchQueue.main.async { [weak self] in
                if let paths = self?.tableView.indexPathsForVisibleRows,
                   paths.contains(indexPath) {
                    self?.tableView.reloadRows(at: [indexPath], with: .none)
                } else {
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    public func update(_ viewModel: TraktViewModel, dataFor indexPath:IndexPath) {
        if let data = viewModel.data.data(forCellAt: indexPath)?.items {
            if let paths = self.tableView.indexPathsForVisibleRows,
               paths.contains(indexPath),
               let cell = self.tableView.cellForRow(at: indexPath) as? MoviesCollectionCell {
                cell.update(data)
            }
        }
    }

    private func didSelect(item: SCCMovie) {
        if item.traktType == .movie ||
            item.traktType == .tvshow{
            viewModel.sccData(for: item)
                .sink { [weak self] maybeData in
                    guard let `self` = self else { return }
                    if let result = maybeData {
                        self.movieDelegate?.traktView(self, didSelect: result)
                    } else {
                        self.showAlert()
                    }
                }.store(in: &cancelables)
        } else {
            viewModel.sccTVShowFromEpisodeData(for: item)
                .sink { [weak self] maybeData in
                    guard let `self` = self else { return }
                    if let result = maybeData {
                        self.movieDelegate?.traktView(self, didSelect: result)
                    } else {
                        self.showAlert()
                    }
                }.store(in: &cancelables)
        }
    }
}

//MARK: - table view delegte
extension TraktViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let viewModel = self.viewModel {
            return viewModel.numberOfRows()
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let viewModel = self.viewModel {
            return viewModel.cellForRow(at: indexPath, for: tableView, delegate: self)
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension TraktViewController: MoviesCollectionCellDelegate {
    func moviesCollectionCell(_ cell:MoviesCollectionCell, didSelect item: SCCMovie) {
        didSelect(item: item)
    }
    
    func moviesCollectionCell(_ cell:MoviesCollectionCell, fetchNextDataFor indexPath:IndexPath?) {
        if let indexPath = indexPath {
            self.viewModel.fetchData(for:indexPath)
        }
    }

    private func showAlert(title:String = "SCC nema data") {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: String(localized: .buttonOK), style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
