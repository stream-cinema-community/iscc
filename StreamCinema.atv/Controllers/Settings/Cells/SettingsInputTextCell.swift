//
//  SettingsInputTextCell.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit


protocol SettingsInputTextCellDelegate {
    func settingsInput(_ cell: SettingsInputTextCell, type:SettingsCellTypes, set value: String)
}


final class SettingsInputTextCell: SettingsCell {
    private let name: UILabel =  UILabel()
    private let valueLabel: UILabel = UILabel()
    private let textField: UITextField = UITextField()

    public var inputDelegate:SettingsInputTextCellDelegate?
    override public var type:SettingsCellTypes? {
        willSet {
            if let type = newValue {
                switch type {
                case .password:
                    self.textField.isEnabled = true
                    self.textField.isSecureTextEntry = true
                case .userName:
                    self.textField.isEnabled = true
                    self.textField.isSecureTextEntry = false
                default:
                    self.textField.isEnabled = false
                }
            }
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }

    public func set(name: String) {
        self.name.text = name
    }

    public func set(value: String) {
        if let type = self.type, type == .password {
            self.valueLabel.text = "••••••••••••"
        } else {
            self.valueLabel.text = value
        }
    }

    private func setupView() {
        self.addSubview(self.name)
        self.addSubview(self.valueLabel)
        self.addSubview(self.textField)
        self.textField.delegate = self

        self.name.translatesAutoresizingMaskIntoConstraints = false
        self.valueLabel.translatesAutoresizingMaskIntoConstraints = false

        self.name.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.name.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        self.name.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        self.name.widthAnchor.constraint(equalToConstant: 600).isActive = true

        self.valueLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        self.valueLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 8 ).isActive = true
        self.valueLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8).isActive = true
        self.valueLabel.leftAnchor.constraint(equalTo: self.name.rightAnchor, constant: 8).isActive = true

    }

    override public func become() {
        self.textField.becomeFirstResponder()
    }

    static func getCell(_ tableView: UITableView) -> SettingsInputTextCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsInputTextCell") as? SettingsInputTextCell {
            return cell
        }
        return SettingsInputTextCell(style: .default, reuseIdentifier: "SettingsInputTextCell")
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        if context.nextFocusedView === self {
            coordinator.addCoordinatedAnimations({
                self.name.textColor = .lightGray
                self.valueLabel.textColor = .lightGray
                self.textField.tintColor = .lightGray
            }, completion: nil)
        }
        else {
            coordinator.addCoordinatedAnimations({
                self.name.textColor = .label
                self.valueLabel.textColor = .label
                self.textField.tintColor = .label
            }, completion: nil)
        }
    }
}

extension SettingsInputTextCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let type = self.type, let text = textField.text {
            self.inputDelegate?.settingsInput(self, type: type, set: text)
        }
    }
}
