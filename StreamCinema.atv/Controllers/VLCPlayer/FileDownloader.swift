//
//  FileDownloader.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 22.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation

final class FileDownloader {

    static func loadFileAsync(url: URL, name:String? = nil, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl = FileManager.default.temporaryDirectory

//        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        var destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)
        if let name = name {
            destinationUrl = documentsUrl.appendingPathComponent("\(name).\(url.pathExtension)")
        }

        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            Log.write("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else
        {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
            {
                data, response, error in
                if error == nil
                {
                    if let response = response as? HTTPURLResponse
                    {
                        if response.statusCode == 200
                        {
                            if let data = data
                            {
                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                {
                                    completion(destinationUrl.path, error)
                                }
                                else
                                {
                                    completion(destinationUrl.path, error)
                                }
                            }
                            else
                            {
                                completion(destinationUrl.path, error)
                            }
                        }
                    }
                }
                else
                {
                    completion(destinationUrl.path, error)
                }
            })
            task.resume()
        }
    }
}
