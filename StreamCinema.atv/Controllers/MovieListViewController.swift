//
//  MovieListViewController.swift
//  MovieDBTV
//
//  Created by Alfian Losari on 23/03/19.
//  Copyright © 2019 Alfian Losari. All rights reserved.
//

import UIKit
import Combine

final class MovieListViewController: UIViewController {

    static let vcIdentifier = "MovieListViewController"
    static func create(appData: AppData) -> MovieListViewController {
        let storyboard = UIStoryboard.main
        let movieVC = storyboard.instantiateViewController(withIdentifier: vcIdentifier) as! MovieListViewController
        movieVC.appData = appData
        return movieVC
    }
    
    @IBOutlet var collectionView: MovieCollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var refreshButton: UIButton!

    // Callbacks - for coordinator
    var onPresentMovieDetailScreen: ((SCCMovie) -> Void)?

    private var appData: AppData!
    private var cancelables: Set<AnyCancellable> = Set()
    private let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()

    private var lastSearchText: String = ""

    var endpoit:TabBarItem? {
        didSet {
            self.type = endpoit?.type
        }
    }
    var actualItem: MenuItem?
    var type: FilterType?
    var model: SCCMovieResult? {
        didSet {
            collectionView.reloadData()
        }
        willSet {
            self.collectionView.model = newValue
            self.activityIndicator.stopAnimating()
        }
    }

    // MARK: - Life-cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.movieDelegate = self
        setupMenuButtonToForceFocusOnTabBar()
        setupSubsriptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        update()
    }
    
    internal func update() {
        guard let actualItem else { return }
        if (model?.data.count ?? 0) <= 100 {
            fetchMovies(for: actualItem, startIndex: 0, for: type)
        }
    }

    private func setupSubsriptions() {
        errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (error) in
                guard let otherError = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherError)
            }.store(in: &cancelables)
    }


    // MARK: - Private methods
    
    private func fetchMovies(for item:MenuItem, startIndex: Int, for type: FilterType?) {
        guard let type = type ?? self.type else { return }
        if item == .genre || item == .friendsAdvise {
            createTableView(for: item)
            return
        } else {
            self.removeGenereView()
        }
        
        self.activityIndicator.startAnimating()

        appData.scService
            .getMovies(type: type, for: item, startIndex: startIndex)
            .assignError(to: errorSubject)
            .handleEvents(receiveCompletion: { [weak self] _ in
                self?.activityIndicator.stopAnimating()
            }).sink { [weak self] data in
                guard let self else { return }
                let isSameFirstMovie = self.model?.data.first == data.data.first
                let isChangedItem = self.actualItem != item
                
                if startIndex == 0 && (isChangedItem || !isSameFirstMovie) {
                    self.update(data: data, startIndex: startIndex)
                } else {
                    self.update(data: data, startIndex: startIndex)
                }
                
            }.store(in: &cancelables)
    }
    
    private func createTableView(for item:MenuItem) {
        if let genereTable = view.subviews.first(where: { $0.isKind(of: GenreTableView.self) }) as? GenreTableView {
            if (item == .genre && ((genereTable.model as? GenreViewModel) != nil)) ||
                (item == .friendsAdvise && ((genereTable.model as? FriendsModel) != nil))
            {
                genereTable.update()
                return
            } else {
                removeGenereView()
            }
        }
        let genreTable = GenreTableView.create(appData: appData, item: item, frame: view.bounds, style: .plain, type: type)
        genreTable.errorSubject
            .sink { [weak self] (error) in
                guard let otherErr = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherErr)
            }.store(in: &cancelables)
        genreTable.type = self.type ?? .movie
        genreTable.genreDelegate = self
        self.model = nil
        view.addSubview(genreTable)
        
        if item == .friendsAdvise, FriendData.friends.count == 0 {
            genreTable.bacgroundText = String(localized: .noFriendsPleaseAddNowInSettings)
        }
        
        genreTable.translatesAutoresizingMaskIntoConstraints = false
        genreTable.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        genreTable.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        genreTable.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        genreTable.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    private func removeGenereView() {
        for view in self.view.subviews where view.isKind(of: GenreTableView.self) {
            view.removeFromSuperview()
        }
    }
    
    private func update(data:SCCMovieResult, startIndex: Int) {
        if startIndex == 0 {
            self.model = data
        } else {
            self.model?.data.append(contentsOf: data.data)
        }
    }
    
    private func showError(_ error: String) {
        infoLabel.text = error
        infoLabel.isHidden = false
        refreshButton.isHidden = false
    }
    
    private func hideError() {
        infoLabel.isHidden = true
        refreshButton.isHidden = true
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
//        guard let item = self.actualItem else { return }
//        self.fetchMovies(for: item)
    }
    
    private func open(movie: SCCMovie) {
        onPresentMovieDetailScreen?(movie)
    }
}

extension MovieListViewController: GenreTableDelegate {
    func castCollectionView(_ castView: GenreTableView, didSelect movie: SCCMovie) {
        self.open(movie: movie)
    }
}

extension MovieListViewController: MenuViewDelegate {
    func menuView(_ menuView: MenuViewController, settingsDid select: SettingsMenuItem) {
        
    }
    
    func menuView(_ menuView: MenuViewController, did select: MenuItem, for type: FilterType?) {
        if self.actualItem != select {
            self.activityIndicator.startAnimating()
            self.hideError()
            self.fetchMovies(for: select, startIndex: 0, for: type)
            self.actualItem = select
        } else {
            update()
        }
    }
}

extension MovieListViewController: MovieCollectionDelegate {
    func movieCollection(_ collection: MovieCollectionView, startIndex: Int) {
        if let item = self.actualItem {
            self.fetchMovies(for: item, startIndex: startIndex, for: type)
        } else if !self.lastSearchText.isEmpty {
            self.search(text: self.lastSearchText, startIndex: startIndex)
        }
    }
    
    func movieCollection(_ collection: MovieCollectionView, didSelect movie: SCCMovie) {
        self.open(movie: movie)
    }
}

extension MovieListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text, !text.isEmpty else {
            self.model = nil
            return
        }
        if self.lastSearchText != text {
            self.search(text: text, startIndex: 0)
        }
        self.lastSearchText = text
    }
    
    func search(text: String, startIndex: Int) {

        self.activityIndicator.startAnimating()
        let filter = FilterModel.allFillter(value: text, startIndex: startIndex)

        appData.scService
            .search(wtih: filter)
            .assignError(to: errorSubject)
            .handleEvents(receiveCompletion: { [weak self] _ in
                self?.activityIndicator.stopAnimating()
            }).sink { [weak self] (data) in
                self?.update(data: data, startIndex: startIndex)
            }.store(in: &cancelables)
    }
    
}


