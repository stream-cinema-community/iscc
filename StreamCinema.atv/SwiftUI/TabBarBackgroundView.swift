//
//  TabBarBackgroundView.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 30.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI
import iSCCCore

struct TabBarBackgroundView: View {

    @State var yAxis: CGFloat =  200
    private var rect: CGRect = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 200, height: 1000))

    var itemWidth: CGFloat = 100
    // Animating Path...

    var animatableData: CGFloat{
        get{ return yAxis }
        set{ yAxis = newValue }
    }

    var itemHalf: CGFloat {
        itemWidth / 2
    }


    var body: some View {
        Path { path in

            let center = yAxis

            path.addRoundedRect(in: .init(x: -15, y: center - itemHalf, width: 35, height: itemWidth), cornerSize: .init(width: 20, height: 20))

        }
        .foregroundColor(Color(sharedColor: .defaultGreen))
        .frame(width: 200, height: 1000)
    }
}

struct TabBarBackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarBackgroundView()
            .previewLayout(.fixed(width: 200, height: 1000))

    }
}
