//
//  MovieIDs.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation
import TraktKit
import CoreData

extension TraktKit.ID {
    public func getIDs() -> IDs {
        return IDs(sccID: nil, trakt: self.trakt, tvdb: self.tvdb, imdb: self.imdb, tmdb: self.tmdb, csfd: nil)
    }
}

extension TraktKit.EpisodeId {
    public func getIDs() -> IDs {
        return IDs(sccID: nil, trakt: self.trakt, tvdb: self.tvdb, imdb: self.imdb, tmdb: self.tmdb, csfd: nil)
    }
}

extension TraktKit.SeasonId {
    public func getIDs() -> IDs {
        return IDs(sccID: nil, trakt: self.trakt, tvdb: self.tvdb, imdb: nil, tmdb: self.tmdb, csfd: nil)
    }
}

extension SupportedServices {
    public func getIDs(with sccID:String?) -> IDs {
        return IDs(sccID: sccID,
                   trakt: (self.trakt != nil) ? Int(self.trakt!) : nil,
                   tvdb: (self.tvdb != nil) ? Int(self.tvdb!) : nil,
                   imdb: self.imdb,
                   tmdb: (self.tmdb != nil) ? Int(self.tmdb!) : nil,
                   csfd: (self.csfd != nil) ? Int(self.csfd!) : nil)
    }
    
}

public struct IDs: Equatable {
    public let sccID: String?
    public let trakt: Int?
    public let tvdb: Int?
    public let imdb: String?
    public let tmdb: Int?
    public let csfd: Int?
    
    public var traktString: String? {
        if let trakt = self.trakt {
            return "\(trakt)"
        }
        return nil
    }
    
    init(sccID: String?, trakt: Int?, tvdb: Int?, imdb: String?, tmdb: Int?, csfd: Int?) {
        self.sccID = sccID
        self.trakt = trakt
        self.tvdb = tvdb
        self.imdb = imdb
        self.tmdb = tmdb
        self.csfd = csfd
    }
    
    init(_ movie: TraktKit.ID) {
        self.trakt = movie.trakt
        self.tvdb = movie.tvdb
        self.imdb = movie.imdb
        self.tmdb = movie.tmdb
        self.sccID = nil
        self.csfd = nil
    }

    init(_ show: TraktKit.EpisodeId) {
        self.trakt = show.trakt
        self.tvdb = show.tvdb
        self.imdb = show.imdb
        self.tmdb = show.tmdb
        self.sccID = nil
        self.csfd = nil
    }
    
    init(with data:NSManagedObject, for type:SCCType) {
        switch type {
        case .movie:
            self.csfd = data.value(forKey: "tvshowCsfd") as? Int
            self.imdb = data.value(forKey: "tvshowImdb") as? String
            self.sccID = data.value(forKey: "tvshowScc") as? String
            self.tmdb = data.value(forKey: "tvshowTmdb") as? Int
            self.trakt = data.value(forKey: "tvshowTrakt") as? Int
            self.tvdb = data.value(forKey: "tvshowTvdb") as? Int
        case .episode:
            self.csfd = data.value(forKey: "episodeCsfd") as? Int
            self.imdb = data.value(forKey: "episodeImdb") as? String
            self.sccID = data.value(forKey: "episodeScc") as? String
            self.tmdb = data.value(forKey: "episodeTmdb") as? Int
            self.trakt = data.value(forKey: "episodeTrakt") as? Int
            self.tvdb = data.value(forKey: "episodeTvdb") as? Int
        case .tvshow:
            self.csfd = data.value(forKey: "tvshowCsfd") as? Int
            self.imdb = data.value(forKey: "tvshowImdb") as? String
            self.sccID = data.value(forKey: "tvshowScc") as? String
            self.tmdb = data.value(forKey: "tvshowTmdb") as? Int
            self.trakt = data.value(forKey: "tvshowTrakt") as? Int
            self.tvdb = data.value(forKey: "tvshowTvdb") as? Int
        case .season:
            self.csfd = data.value(forKey: "seasonCsfd") as? Int
            self.imdb = data.value(forKey: "seasonImdb") as? String
            self.sccID = data.value(forKey: "seasonScc") as? String
            self.tmdb = data.value(forKey: "seasonTmdb") as? Int
            self.trakt = data.value(forKey: "seasonTrakt") as? Int
            self.tvdb = data.value(forKey: "seasonTvdb") as? Int
        }
    }
    
    public static func == (lhs: IDs, rhs: IDs) -> Bool {
        if let lhsScc = lhs.sccID,
           let rhsScc = rhs.sccID {
            return lhsScc == rhsScc
        }
        if let lhsIMDB = lhs.imdb,
           let rhsIMDb = rhs.imdb {
            return lhsIMDB == rhsIMDb
        }
        if let lhsTMDB = lhs.tmdb,
           let rhsTMDB = rhs.tmdb {
            return lhsTMDB == rhsTMDB
        }
        return lhs.trakt == rhs.trakt
    }
}
