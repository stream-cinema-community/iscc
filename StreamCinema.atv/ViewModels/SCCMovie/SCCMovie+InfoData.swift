//
//  SCCMovie+InfoData.swift
//  StreamCinema.atv
//
//  Created by SCC on 30/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

//MARK: - SCC data parse
extension InfoData {
    func getSCCMovie(totalCount: Int) -> SCCMovie {
        guard let id = self.id,
              let infoLabel = self.source?.getInfoLabels(),
              let type = self.source?.infoLabels?.getMediaType()
        else { return SCCMovie() }
        
        let desc = infoLabel.plot
        var title = infoLabel.title
        let currentIDs = self.source?.services?.getIDs(with: id)
        var rootIDs:IDs?
        var seasonIDs:IDs?
        var episodeIDs:IDs?
        
        switch type {
        case .movie,
             .tvshow:
            rootIDs = currentIDs
        case .season:
            if let seasionNumber = self.source?.infoLabels?.season {
                title = "\(String(localized: .season)): \(seasionNumber) "
            }
            seasonIDs = currentIDs
        case .episode:
            if title == nil {
                title = String(localized: .episode)
                if let episodeNumber = self.source?.infoLabels?.episode {
                    title = "\(episodeNumber) - \(String(localized: .episode))"
                }
            }
            episodeIDs = currentIDs
        }
        
        var posterUrl:URL?
        if var posterString = infoLabel.art?.poster,
           !posterString.contains("null") {
            posterString.findAndReplace("//")
            posterUrl = posterString.createURL
        }
        
        var bannerUrl:URL?
        if var bannerString = infoLabel.art?.banner,
           !bannerString.contains("null") {
            bannerString.findAndReplace("//")
            bannerUrl = bannerString.createURL
        }
        
        var fanArtURL:URL?
        if var fanartString = infoLabel.art?.fanart,
           !fanartString.contains("null") {
            fanartString.findAndReplace("//")
            fanArtURL = fanartString.createURL
        }
        
        var clearlogoURL:URL?
        if var clearlogoString = infoLabel.art?.clearlogo,
           !clearlogoString.contains("null") {
            clearlogoString.findAndReplace("//")
            clearlogoURL = clearlogoString.createURL
        }
        
        var thumbURL: URL?
        if var thumb = infoLabel.art?.thumb,
           !thumb.contains("null") {
            thumb.findAndReplace("//")
            thumbURL = thumb.createURL
        }
        
        if fanArtURL == nil,
           var fanartOirg = infoLabel.art?.fanartOirg,
          !fanartOirg.contains("null") {
            fanartOirg.findAndReplace("//")
            fanArtURL = fanartOirg.createURL
        } else if thumbURL == nil,
            var fanartOirg = infoLabel.art?.fanartOirg,
           !fanartOirg.contains("null") {
            fanartOirg.findAndReplace("//")
            thumbURL = fanartOirg.createURL
            
        }
        if thumbURL == nil,
            var landscape = infoLabel.art?.landscape,
           !landscape.contains("null") {
            landscape.findAndReplace("//")
            thumbURL = landscape.createURL
            
        }
        
        
        let audio = self.source?.availableStreams?.languages?.audio?.map
        
        let movieData = self.source?.getMovieData() ?? [[:]]
        let ratingData = self.source?.getRating() ?? Rating()
        let trailer = self.source?.getTrailer()
        let episode = self.source?.infoLabels?.episode
        let season = self.source?.infoLabels?.season
        var date: Date?
        if let dateString = source?.infoLabels?.premiered {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yy-MM-dd"
            date = dateFormatter.date(from: dateString)
        }
        
        let cast = self.source?.cast
        let director = source?.infoLabels?.director
        let studio = source?.infoLabels?.studio
        let writers = source?.infoLabels?.writer
        
        var movie = SCCMovie(title: (title ?? ""), traktType: type.sccT, rating: ratingData, auiodLangs: audio,
                             rootIDs: rootIDs, seasonIDs: seasonIDs, episodeIDs: episodeIDs,
                             season: season, episode: episode, poster: posterUrl, banner: bannerUrl, fanart: fanArtURL,
                             logo: clearlogoURL, thumb: thumbURL, date: date, trailer: trailer, desc: desc,
                             movieData: movieData, state: WatchedState.none, cast:cast,
                             progress: nil, time: nil, year: self.source?.infoLabels?.year, director: director, studio: studio,
                             writers: writers, totalCount: totalCount, duration: source?.infoLabels?.duration)
        #if TARGET_OS_TVOS
        movie.uprdateProgressFromSavedScc()
        #endif
        return movie
    }
}

fileprivate extension String {
    mutating
    func findAndReplace(_ string:String) {
        if self.starts(with: string) {
            let range = self.range(of: string)
            self = self.replacingOccurrences(of: string, with: "", range: range)
        }
    }
    
    var createURL: URL? {
        if starts(with: "http://") || starts(with: "https://") {
            return URL(string: self)
        }
        else {
            return URL(string: "http://" + self)
        }
    }
}
