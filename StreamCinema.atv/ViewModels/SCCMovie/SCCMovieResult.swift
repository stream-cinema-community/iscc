//
//  SCCMovieResult.swift
//  StreamCinema.atv
//
//  Created by SCC on 30/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

struct SCCMovieResult {
    var data: [SCCMovie]
    var startIndex: Int
    var totalCount: Int
    
    var isExistsNextData: Bool {
        get {
            data.count < totalCount
        }
    }
    
    init(data: [SCCMovie], startIndex: Int, totalCount: Int) {
        self.data = data
        self.startIndex = startIndex
        self.totalCount = totalCount
    }
    
    init(_ resultData:FilterResult) {
        self.startIndex = resultData.hits.data?.count ?? 1
        self.totalCount = resultData.hits.total.value
        if var data = resultData.hits.data {
            #if TARGET_OS_TVOS
            if let isDisabled = (UIApplication.shared.delegate as? AppDelegate)?.appData.isDisableAdult,
               isDisabled == true {
                data = data.filter { movie -> Bool in
                    if let adult = movie.source?.infoLabels?.genre,
                       adult.contains(where: { Genere.explicitGenere.contains($0) }) {  
                        return false
                    }
                    return true
                }
            }
            #endif
            let mapedData = data.map { infoData -> SCCMovie in
                return infoData.getSCCMovie(totalCount:resultData.hits.total.value)
            }
            self.data = mapedData
        } else {
            self.data = []
        }
    }
    
    init() {
        self.startIndex = 0
        self.totalCount = 0
        self.data = []
    }
    
    mutating func sortByDateCreate() {
        if data.count == totalCount {
            data.sort { lhs, rhs in
                if (lhs.year ?? 0) == (rhs.year ?? 0) {
                    return (lhs.date?.timeIntervalSince1970 ?? 0) > (rhs.date?.timeIntervalSince1970 ?? 0)
                }
                else {
                    return (lhs.year ?? 0) > (rhs.year ?? 0)
                }
            }
        }
    }
}
