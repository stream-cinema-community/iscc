//
//  SCCMovie+Images.swift
//  StreamCinema.atv
//
//  Created by SCC on 30/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import TMDBKit
import FanArtKit

//MARK: - Fanart and TMDB images
extension SCCMovie {
    public func bacgroundImage() async -> URL? {
        return await withUnsafeContinuation ({ continuation in
            cellBacgroundImage { url in
                continuation.resume(returning: url)
            }
        })
    }
    
    public func cellBacgroundImage(completition: @escaping (URL?)->Void) {
        if let banner {
            completition(banner)
        }
        else if let thumb = self.thumb {
            completition(thumb)
        }
        else if let fanArt = self.fanart {
            completition(fanArt)
        } else {
            fetchBacgroundImage(completition: completition)
        }
    }
    
    private func fetchBacgroundImage(completition: @escaping (URL?)->Void) {
        if let tmdbID = self.ids?.tmdb {
            tmdbBackdrop(tmdbID: tmdbID) { url in
                if let url = url {
                    completition(url)
                } else if let tmdbID = self.rootIDs?.tmdb {
                    tmdbBackdrop(tmdbID: tmdbID, completition: completition)
                } else {
                    fanArtThumb(completition: completition)
                }
            }
        } else {
            completition(nil)
        }
    }
    
    private func tmdbBackdrop(tmdbID: Int, completition: @escaping (URL?)->Void ) {
        
        if self.traktType == .movie {
            MovieMDB.images(movieID: tmdbID, language: nil) { (value, images) in
                self.tmdbImage(value, images, completition: completition)
            }
        } else if self.traktType == .episode,
                  let tmdb = self.rootIDs?.tmdb,
                  let episode = self.episode,
                  let season = self.season {
            TVEpisodesMDB.images(tvShowId: tmdb, seasonNumber: season, episodeNumber: episode) { (value, images) in
                self.tmdbImage(value, images, completition: completition)
            }
        } else {
            TVMDB.images(tvShowID: tmdbID, language: nil) { (value, images) in
                self.tmdbImage(value, images, completition: completition)
            }
        }
    }
    
    private func tmdbImage(_ value: ClientReturn, _ images: ImagesMDB?, completition: @escaping (URL?)->Void ) {
        if let backdrop = value.json?["backdrop_path"],
           backdrop != nil,
           let backdropURL = URL(string: "https://image.tmdb.org/t/p/w500\(backdrop)"){
            completition(backdropURL)
        }
        else if let thumb = images?.backdrops.first?.file_path,
           let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(thumb)") {
            completition(backdrop)
        }
        else if let stillPath = images?.stills.first?.file_path,
                let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(stillPath)") {
            completition(backdrop)
        }
        else if let posters = images?.posters.first?.file_path,
                let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(posters)") {
            completition(backdrop)
        }
        else {
            completition(nil)
        }
    }
    
    private func fanArtThumb(completition: @escaping (URL?)->Void) {
        if let tmdbID = self.ids?.tmdb {
            fanArtThumb(tmdbID: tmdbID) { url in
                if let url {
                    completition(url)
                } else if let tmdbID = self.rootIDs?.tmdb {
                    fanArtThumb(tmdbID: tmdbID, completition: completition)
                } else {
                    completition(nil)
                }
            }
        } else {
            completition(nil)
        }
    }
    
    private func fanArtThumb(tmdbID: Int, completition: @escaping (URL?)->Void) {
        if self.traktType == .movie {
            FanartClient.shared.movie(tmdb: tmdbID) { fanArtResult -> (Void) in
                switch fanArtResult {
                case .success(result: let result):
                    if let strURL = result.moviethumb?.first?.url {
                        completition(URL(string: strURL))
                    } else {
                        completition(nil)
                    }
                case .error(reason: let reason):
                    Log.write("fanArtThumb movie reason \(reason)")
                    completition(nil)
                }
            }
        } else {
            let tvDB = rootIDs?.tvdb ?? tmdbID
            FanartClient.shared.show(identifier: tvDB) { fanArtResult -> (Void) in
                switch fanArtResult {
                case .success(result: let result):
                    if let strURL = result.tvthumb?.first?.url {
                        completition(URL(string: strURL))
                    } else {
                        completition(nil)
                    }
                case .error(reason: let reason):
                    Log.write("fanArtThumb show reason \(reason)")
                    completition(nil)
                }
            }
        }
    }
}

extension SCCMovie {
    func movieShowBackdrop() async -> URL? {
        guard let tmdbID = rootIDs?.tmdb
        else { return nil }
        return await withUnsafeContinuation ({ continuation in
            MovieMDB.images(movieID: tmdbID) { clientReturn, images in
                continuation.resume(returning: tmdbImage(clientReturn, images))
            }
        })
    }
    
    
    func episodeBackdrop() async -> URL? {
        guard let tvshowID = rootIDs?.tmdb,
              let seasonNumber = season,
              let episodeNumber = episode
        else { return nil }
        
        return await withUnsafeContinuation ({ continuation in
            TVEpisodesMDB.images(tvShowId: tvshowID, seasonNumber: seasonNumber, episodeNumber: episodeNumber) { (value, images) in
                continuation.resume(returning: tmdbImage(value, images))
            }
        })
    }
    
    private func tmdbImage(_ value: ClientReturn, _ images: ImagesMDB? ) -> URL? {
        if let backdrop = value.json?["backdrop_path"],
           backdrop != nil,
           let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(backdrop)"){
            return backdrop
        }
        else if let thumb = images?.backdrops.first?.file_path,
           let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(thumb)") {
            return backdrop
        }
        else if let stillPath = images?.stills.first?.file_path,
                let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(stillPath)") {
            return backdrop
        }
        else {
            return nil
        }
    }
}

extension SCCMovie {
    func asyncLogo() async -> URL? {
        guard traktType == .movie || traktType == .tvshow else { return nil }
        return await withUnsafeContinuation ({ continuation in
            if self.traktType == .movie  {
                guard let tmdbid = self.rootIDs?.tmdb else { return }
                FanartClient.shared.movie(tmdb: tmdbid) { result in
                    switch result {
                    case .success(result: let ranart):
                        continuation.resume(returning: ranart.findLogoUrl)
                    case .error:
                        continuation.resume(returning: nil)
                    }
                }
            } else  {
                guard let tmdbid = self.rootIDs?.tvdb else { return }
                FanartClient.shared.show(identifier: tmdbid) { result in
                    switch result {
                    case .success(result: let ranart):
                        continuation.resume(returning: ranart.findLogoUrl)
                    case .error:
                        continuation.resume(returning: nil)
                    }
                }
            }
        })
    }
}

extension FanArtTVShow {
    var findLogoUrl: URL? {
        if let logo = clearlogo?.first(where: { $0.lang == "sk" }) {
            return logo.urlLink
        }
        if let art = clearart?.first(where: { $0.lang == "sk" }) {
            return art.urlLink
        }
        if let logo = clearlogo?.first(where: { $0.lang == "en" }) {
            return logo.urlLink
        }
        if let art = clearart?.first(where: { $0.lang == "en" }) {
            return art.urlLink
        }
        if let logo = clearlogo?.first(where: { $0.lang != "ru" }) {
            return logo.urlLink
        }
        if let art = clearart?.first(where: { $0.lang != "ru" }) {
            return art.urlLink
        }
        return clearlogo?.first?.urlLink ?? clearart?.first?.urlLink
    }
}

extension FanArtMovie {
    var findLogoUrl: URL? {
        if let logo = movielogo?.first(where: { $0.lang == "sk" }) {
            return logo.urlLink
        }
        if let art = hdmovieclearart?.first(where: { $0.lang == "sk" }) {
            return art.urlLink
        }
        if let logo = movielogo?.first(where: { $0.lang == "en" }) {
            return logo.urlLink
        }
        if let art = hdmovieclearart?.first(where: { $0.lang == "en" }) {
            return art.urlLink
        }
        if let logo = movielogo?.first(where: { $0.lang != "ru" }) {
            return logo.urlLink
        }
        if let art = hdmovieclearart?.first(where: { $0.lang != "ru" }) {
            return art.urlLink
        }
        return movielogo?.first?.urlLink ?? hdmovieclearart?.first?.urlLink
    }
}

extension Hdmovieclearart {
    var urlLink: URL? {
        if let url {
            return URL(string: url)
        }
        return nil
    }
}

extension Characterart {
    var urlLink: URL? {
        if let url {
            return URL(string: url)
        }
        return nil
    }
}
