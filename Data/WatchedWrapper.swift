//
//  WatchedWrapper.swift
//  StreamCinema.atv
//
//  Created by SCC on 17/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation

public final class WatchedWrapper: NSObject {
    static func movie(movieID:String?, traktID:String?) -> SCCMovie? {
        let traktW = WatchedData.getMovie(traktID, entity: .trakt)
        let sccW = WatchedData.getMovie(movieID, entity: .scc)
        
        return WatchedWrapper.resolve(sccW, traktW)
    }
    
    static func show(movieID:String?, traktID:String?) -> SCCMovie? {
        let traktW = WatchedData.getShow(traktID, entity: .trakt)
        let sccW = WatchedData.getShow(movieID, entity: .scc)
        
        return WatchedWrapper.resolve(sccW, traktW)
    }
    
    static func lastPlayedEpisode(for showIDs:IDs?) -> SCCMovie? {
        let sccW = WatchedData.lastEpisode(showID: showIDs?.sccID, entity: .scc)
        let traktW = WatchedData.lastEpisode(showID: showIDs?.traktString, entity: .trakt)
        if let sccWDate = sccW?.date,
           let traktWDate = traktW?.date,
           sccWDate > traktWDate {
            return sccW
        }
        if let traktW = traktW {
            return traktW
        }
        return sccW
    }
    
    static func episode(movieID:String?, traktID:String?) -> SCCMovie? {
        let traktW = WatchedData.getEpisode(traktID, entity: .trakt)
        let sccW = WatchedData.getEpisode(movieID, entity: .scc)
        return WatchedWrapper.resolve(sccW, traktW)
    }
    
    static func season(movieID:String?, traktID:String?) -> SCCMovie? {
        let traktW = WatchedData.getSeason(movieID, entity: .trakt)
        let sccW = WatchedData.getSeason(movieID, entity: .scc)
        return WatchedWrapper.resolve(sccW, traktW)
    }
    
    static func existWatchedEpisode(for show:IDs?) -> Bool {
        if let sccID = show?.sccID,
           WatchedData.existWatchedEpisode(for: sccID, entity: .scc) {
            return true
        }
        if let traktID = show?.trakt,
           WatchedData.existWatchedEpisode(for: "\(traktID)", entity: .trakt) {
            return true
        }
        return false
    }
    
    static func allEpisodesFor(movieID:String?, traktID:String?) -> [SCCMovie] {
        var traktW = WatchedData.getAllEpisodes(for: traktID, entity: .trakt)
        var sccW = WatchedData.getAllEpisodes(for: movieID, entity: .scc)
        
        if sccW.count > 0 {
            for watched in sccW {
                if let episodeTraktID = watched.ids?.trakt {
                    let traktIndex = traktW.firstIndex(where: { trakt -> Bool in
                        return trakt.ids?.trakt == episodeTraktID
                    })
                    if let traktIndex = traktIndex {
                        traktW.remove(at: traktIndex)
                    }
                }
            }
        }
        sccW.append(contentsOf: traktW)
        return sccW
    }
    
    public static func deleteAllData(for entity:WatchedEntity = .scc) {
        WatchedData.deleteAllData(for: entity)
        if entity == .scc {
            WatchedData.deleteAllData(for: WatchedEntity.sccWatchList)
        }
    }
    
    
    static func getWatched(type: FilterType, entity:WatchedEntity = .scc ) -> [SCCMovie] {
        if type == .movie {
            return WatchedData.getAllMovies(entity: entity)
        } else if type == .tvshow {
            return WatchedData.getAllShows(entity: entity)
        } else {
            return []
        }
    }
    
    private static func resolve(_ sccW:SCCMovie?, _ traktW: SCCMovie?) -> SCCMovie? {
        if let traktW = traktW,
           var sccW = sccW {
            sccW.mearge(with: traktW)
            return sccW
        } else if let sccW = sccW {
            return sccW
        } else if let traktW = traktW {
            return traktW
        }
        return nil
    }
}
