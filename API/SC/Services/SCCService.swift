//
//  SCCService.swift
//  StreamCinema.atv
//
//  Created by iSCC on 30/12/2022.
//  Copyright © 2022 SCC. All rights reserved.
//

import Foundation

class SCCService {
    
    enum SC2Error: Error {
        case genreMovieFilterNotSpecified
        case getAllWatchedDataFailed
        case unknownTraktError
        case unknownTMDBError
    }
    
    
    let apiClient = Provider.sc2Request
}
