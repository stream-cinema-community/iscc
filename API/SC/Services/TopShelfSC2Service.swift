//
//  TopShelfSC2Service.swift
//  StreamCinema.atv
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import Combine

final class TopShelfSC2Service: SCCService {

    public func getTopShelfData(type: FilterType) -> AnyPublisher<SCCMovieResult, Error> {
        let filter = FilterModel.lastReleasedDubbed(type: type, startIndex: 0, offSet: 10)
        return self.filter(with: filter)
    }
    
    private func filter(with model: FilterModel) -> AnyPublisher<SCCMovieResult, Error> {
       apiClient
                .request(request: SC2Request.filter(with: model), model: FilterResult.self)
                .map { SCCMovieResult($0) }
                .eraseToAnyPublisher()
    }
}

