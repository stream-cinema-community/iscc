//
//  FilterModel.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

struct FilterModel {
    var name:  FilterName
    var type:  FilterType
    var order: FilterOrder
    var sort:  FilterSort?
    
    var count: FilterCount?
    var lang: [String]?
    var value: [String]?
    var service: FilterService?
    var days: String?
    var genre: Genere?
    var episode: String?
    var season: String?
    var root_parent: String?
    
    var startIndex: Int = 0
    var offSet: Int
    
    var isV2Filter: Bool {
        switch name {
        case .search:
            return true
        case .startsWithSimple:
            return false
        case .genre:
            return true
        case .all:
            return true
        case .service:
            return true
        case .ids:
            return true
        case .parent:
            return true
        case .studio:
            return true
        case .newsDubbed:
            return true
        case .news:
            return true
        case .country:
            return true
        case .year:
            return true
        case .language:
            return true
        case .numbering:
            return true
        case .concert:
            return true
        }
    }
    
    static func initTaktFilter(value: [String], startIndex: Int,  offSet: Int = 100) -> FilterModel {
        let filter = FilterModel(name: .service, type: .all, order: .desc, sort: nil, count:nil, lang: nil, value: value, service: .traktWithType, days: nil, startIndex: startIndex, offSet: offSet)
        return filter
    }
    
    static func initTmdbFilter(value: Int, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        let filter = FilterModel(name: .service, type: .all, order: .desc, sort: nil, count:nil, lang: nil, value: ["\(value)"], service: .tmdb, days: nil, startIndex: startIndex, offSet: offSet)
        return filter
    }
    
    static func initImdbFilter(value: String, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        let filter = FilterModel(name: .service, type: .all, order: .desc, sort: nil, count:nil, lang: nil, value: [value], service: .imdb, days: nil, startIndex: startIndex, offSet: offSet)
        return filter
    }
    
    static func azInit(type: FilterType = .movie, value: String? = nil, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        var newValue:[String] = []
        if value != nil {
            newValue.append(value!)
        }
        let filter = FilterModel(name: .startsWithSimple, type: type, order: .desc, sort: .title, count: .titles, lang: nil, value: newValue, service: nil, days: nil, startIndex: startIndex, offSet: offSet)
        return filter
    }

    static func initStudios(type: FilterType = .all, startIndex: Int, offSet: Int = 100) -> FilterModel {
        let newValue:[String] = [""]
        let filter = FilterModel(name: .startsWithSimple, type: type, order: .desc, sort: .playCount, count: .studios, lang: nil, value: newValue, service: nil, days: nil, startIndex: startIndex, offSet: offSet)
        return filter
    }
    
    static func searchStartSimpleInit(type: FilterType = .movie, value: String? = nil, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        var newValue:[String] = []
        if value != nil {
            newValue.append(value!)
        }
        let filter = FilterModel(name: .startsWithSimple, type: type, order: .desc, sort: nil, count: nil, lang: nil, value: newValue, service: nil, days: nil, startIndex: startIndex, offSet: offSet)
        return filter
    }
    
    static func genere(value:FindingValue, type:FilterType, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .genre,
                           type: type,
                           order: .desc,
                           sort: .trending,
                           count: nil,
                           lang: nil,
                           value: [value.value],
                           service: nil,
                           days: nil,
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func allFillter(value:String, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .search,
                           type: .all,
                           order: .desc,
                           sort: .score,
                           count: nil,
                           lang: nil,
                           value: [value],
                           service: nil,
                           days: nil,
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func sccIDsFilter(sccIDs:[String], type: FilterType, sort:FilterSort? = nil, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .ids,
                           type: type,
                           order: .desc,
                           sort: sort,
                           count: nil,
                           lang: nil,
                           value: sccIDs,
                           service: nil,
                           days: nil,
                           genre: nil, startIndex: startIndex, offSet: offSet)
    }
    
    static func csfdFillter(serviceID:[String], type: FilterType, sort:FilterSort? = nil, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .service,
                           type: type,
                           order: .desc,
                           sort: sort,
                           count: nil,
                           lang: nil,
                           value: serviceID,
                           service: .csfd,
                           days: nil,
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func seasonFilter(mediaID:String, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .parent,
                           type: .season,
                            order: .asc,
                            sort: .episode,
                            count: nil,
                            lang: nil,
                            value: [mediaID],
                            service: nil,
                            days: nil,
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func popularFilter(type: FilterType, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .all,
                                type: type,
                                order: .desc,
                                sort: .playCount,
                                count: nil,
                                lang: nil,
                                value: nil,
                                service: nil,
                                days: "365",
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func newsFilter(type: FilterType, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .news,
                            type: type,
                            order: .desc,
                            sort: .dateAdded,
                            count: nil,
                            lang: nil,
                            value: nil,
                            service: nil,
                            days: "365",
                           startIndex: startIndex, offSet: offSet)
    }
    /// notwork
    static func lastAddedDubbedFilter(type: FilterType, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .all,
                            type: type,
                            order: .desc,
                            sort: .dateAdded,
                            count: nil,
                            lang: ["cs","sk"],
                            value: nil,
                            service: nil,
                            days: nil,
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func lastAddedFilter(type: FilterType, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .all,
                            type: type,
                            order: .desc,
                            sort: .dateAdded,
                            count: nil,
                            lang: nil,
                            value: nil,
                            service: nil,
                            days: nil,
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func trending(type: FilterType, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .all,
                        type: type,
                        order: .desc,
                        sort: .trending,
                        count: nil,
                        lang: nil,
                        value: nil,
                        service: nil,
                        days: nil,
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func popular(type: FilterType, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .all,
                        type: type,
                        order: .desc,
                        sort: .popularity,
                        count: nil,
                        lang: nil,
                        value: nil,
                        service: nil,
                        days: nil,
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func lastMostWatched(type: FilterType, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .all,
                        type: type,
                        order: .desc,
                        sort: .playCount,
                        count: nil,
                        lang: ["cs","sk"],
                        value: nil,
                        service: nil,
                        days: "7",
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func lastReleasedDubbed(type: FilterType, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .newsDubbed,
                        type: type,
                        order: .desc,
                        sort: .langDateAdded,
                        count: nil,
                        lang: ["cs","sk"],
                        value: nil,
                        service: nil,
                        days: "730",
                           startIndex: startIndex, offSet: offSet)
    }
    
    static func getNext(episode:String, season:String, root_parent:String, startIndex: Int,  offSet: Int = 100) -> FilterModel {
        return FilterModel(name: .numbering,
                           type: .all,
                        order: .desc,
                        sort: nil,
                        count: nil,
                        lang: nil,
                        value: nil,
                        service: nil,
                        days: nil,
                        episode: episode,
                        season: season,
                        root_parent: root_parent, startIndex: startIndex, offSet: offSet)
    }
    
    func getDictionnary() -> [String:Any]{
        var dict: [String: Any] = [:]

        dict["order"] = self.order.rawValue
        dict["type"] = self.type.rawValue
        
        if self.sort != nil {
            dict["sort"] = self.sort!.rawValue
        }
        if let lang = self.lang {
            dict["lang"] = lang
        }
        if self.value != nil,
           self.name != .ids {
            dict["value"] = self.value
        } else if self.name != .ids {
            dict["value"] = ""
        } else if self.name == .ids {
            dict["id"] = self.value
        }
        if self.service != nil {
            dict["service"] = self.service?.rawValue
        }
        if self.days != nil {
            dict["days"] = self.days
        }
        
        if let token = Bundle.main.infoDictionary?["CFBundleSCC"]  {
            dict["access_token"] = token
        }
        
        if let season = self.season {
            dict["season"] = season
        }
        
        if let episode = self.episode {
            dict["episode"] = episode
        }
        
        if let root_parent = self.root_parent {
            dict["root_parent"] = root_parent
        }
        dict["from"] = startIndex
        dict["size"] = offSet
        return dict
    }
    
}

enum FilterService: String {
    case csfd
    case imdb
    case traktWithType = "trakt_with_type"
    case tvdb
    case slug
    case tmdb
    case fanart
    case trakt
}

enum FilterName: String {
    case search
    case startsWithSimple
    case genre
    case all
    case service
    case ids
    case parent
    case studio
    case newsDubbed
    case news
    case country
    case year
    case language
    case numbering
    case concert
}

enum FilterSort: String {
    case score
    case year
    case premiered
    case dateAdded
    case title
    case playCount
    case lastSeen
    case episode
    case news
    case popularity
    case trending
    case langDateAdded

}

enum FilterOrder: String {
    case asc
    case desc
}

enum FilterType: String {
    case movie
    case tvshow
    case concert
    case season
    case episode
    case all = "*"
    case trakt
}

enum FilterCount: String {
    case titles
    case genres
    case studios
    case countries
}
