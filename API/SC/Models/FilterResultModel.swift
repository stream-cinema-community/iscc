//
//  FilterResultModel.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//
import Foundation


struct FilterResult: DataConvertible, Hashable, Model {
    let took: Int?
    let timedOut: Bool?
    let shards: Shards?
    var hits: Hits

    enum CodingKeys: String, CodingKey {
        case took
        case timedOut = "timed_out"
        case shards = "_shards"
        case hits
    }
    
    static func empty<T>() -> T where T: Model {
        let filterResult = FilterResult(took: 0, timedOut: false, shards: Shards.empty(), hits: Hits.empty())
        guard let emptyFilterResult = filterResult as?T else {
            fatalError()
        }
        return emptyFilterResult
    }
}

// MARK: - Hits
struct Hits: DataConvertible, Hashable, Model {
    var total: Total
    var data: [InfoData]?

    enum CodingKeys: String, CodingKey {
        case total
        case data = "hits"
    }
    
    static func empty<T>() -> T where T : Model {
        let hits = Hits(total: Total.empty(), data: [])
        guard let emptyHits = hits as?T else {
            fatalError()
        }
        return emptyHits
    }
}

// MARK: - Total
struct Total: DataConvertible, Hashable, Model {
    var value: Int
    let relation: String
    
    static func empty<T>() -> T where T : Model {
        let total = Total(value: 0, relation: "0")
        guard let emptyTotal = total as?T else {
            fatalError()
        }
        return emptyTotal
    }
}

// MARK: - Shards
struct Shards: DataConvertible, Hashable, Model {
    let total, successful, skipped, failed: Int
    
    static func empty<T>() -> T where T : Model {
        let shards = Shards(total: 0, successful: 0, skipped: 0, failed: 0)
        guard let emptyShards = shards as?T else {
            fatalError()
        }
        return emptyShards
    }
}

//struct FilterResult: DataConvertible, Hashable, Model {
//    var pagination: Pagination?
//    let totalCount: Int?
//    var data: [InfoData]?
//
//    enum CodingKeys: String, CodingKey {
//        case pagination = "pagination"
//        case totalCount = "totalCount"
//        case data = "data"
//    }
//
//    static func empty<T>() -> T where T: Model {
//        let filterResult = FilterResult(pagination: Pagination.empty(), totalCount: 0, data: [])
//        guard let emptyFilterResult = filterResult as?T else {
//            fatalError()
//        }
//        return emptyFilterResult
//    }
//}
