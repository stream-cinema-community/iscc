//
//  CustomFilterModel.swift
//  StreamCinema.atv
//
//  Created by iSCC on 17/12/2022.
//  Copyright © 2022 SCC. All rights reserved.
//

import Foundation

enum MovieStaffType {
    case directors
    case writers
    case studios
    
    var title: String {
        switch self {
        case .directors:
            return "Director"
        case .writers:
            return "Writer"
        case .studios:
            return "Studio"
        }
    }
}

import TMDBKit
import Combine
final class MovieStaff {
    let type: MovieStaffType
    let name: String
    var image: URL?
    
    init(type: MovieStaffType, name: String, image: URL? = nil) {
        self.type = type
        self.name = name
        self.image = image
    }
    
    func findImageForCurrentResult() -> AnyPublisher<URL?, Error> {
        let subject: PassthroughSubject<URL?, Error> = PassthroughSubject()
        
        SearchMDB.company(query: name, page: nil) { [weak self] clientReturn, company in
            if let company = company?.filter({ $0.logo_path != nil }).first,
               let logoPathUrl = URL(string: "https://image.tmdb.org/t/p/w500\(company.logo_path!)") {
                self?.image = logoPathUrl
                subject.send(logoPathUrl)
            } else if let error = clientReturn.error {
                subject.send(completion: .failure(error))
            } else {
                subject.send(nil)
            }
        }
        return subject.first().eraseToAnyPublisher()
    }
}

struct CustomFilterModel {
    var sortConfig: String
    var config: String
    var startIndex: Int
    var offSet: Int = 100
    
    init(sortConfig: String, config: String, startIndex: Int) {
        self.sortConfig = sortConfig
        self.config = config
        self.startIndex = startIndex
    }
    
    init(with staff: MovieStaff, startIndex: Int) {
        self.sortConfig = ""
        self.config = ""
        self.startIndex = startIndex
    }
    
    init(with castName:String, startIndex: Int) {
        self.sortConfig = ""
        self.config = ""
        self.startIndex = startIndex
    }
    
}
