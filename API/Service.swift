//
//  MovieService.swift
//  StreamCinema
//
//  Created by SCC on 23/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import CombineMoya
import Combine

var cancelables: Set<AnyCancellable> = Set()

extension MoyaProvider {

    func requestData(request: Target) -> AnyPublisher<Data, Error> {
        requestWithoutMapping(request: request)
        .catchCommonErrors()
        .tryMap({ response in
            if let error = self.resutlStatus(response.statusCode) {
                throw error
            }
            return response.data
        })
        .eraseToAnyPublisher()
    }
    
    func requestAsync<T: Model>(_ target: Target, model: T.Type) async throws -> T {
        var data = try await requestDataAsync(target)
        
        if var convertedString = String(data: data, encoding: String.Encoding.utf8) {
            if convertedString.prefix(1) == "[" {
                convertedString = "{ \"data\": " + convertedString + "}"
            }
            if let updatedData = convertedString.data(using: .utf8) {
                data = updatedData
            }
        }
        
        return try JSONDecoder().decode(T.self, from: data)
    }
    
    func requestDataAsync(_ target: Target) async throws -> Data {
        return try await withCheckedThrowingContinuation({ continuation in
            request(target) { result in
                switch result {
                case .success(let moyaResponse):
                    guard let statusCode = HTTPStatusCode(rawValue: moyaResponse.statusCode)
                        else {
                        continuation.resume(throwing: CommonErrors.serverNotResponding )
                        return
                    }
                    
                    if statusCode.responseType == .success {
                        continuation.resume(returning: moyaResponse.data)
                    } else if statusCode.responseType == .clientError {
                        continuation.resume(throwing: statusCode)
                    } else if statusCode.responseType == .serverError {
                        continuation.resume(throwing: CommonErrors.serverNotResponding)
                    } else {
                        continuation.resume(throwing: CommonErrors.unownedStatusCode)
                    }
                case .failure(let error):
                    continuation.resume(throwing: error)
                }
            }
        })
    }
//case .success(let moyaResponse):
//    guard let statusCode = HTTPStatusCode(rawValue: moyaResponse.statusCode)
//    else { throw CommonErrors.serverNotResponding }
//
//    if statusCode.responseType == .success {
//        continuation.resume(returning: moyaResponse.data)
//    } else if statusCode.responseType == .serverError {
//        continuation.resume(throwing: CommonErrors.serverNotResponding)
//    } else {
//        continuation.resume(throwing: CommonErrors.unownedStatusCode)
//    }
//case .failure(let error):
//    continuation.resume(throwing: error)
//}
    @discardableResult
    func request(_ target: Target) -> AnyPublisher<Data,Error> {
        return requestWithoutMapping(request: target)
            .catchCommonErrors()
            .tryMap({ response in
                if let error = self.resutlStatus(response.statusCode) {
                    throw error
                }
                return response.data
            })
            .eraseToAnyPublisher()
    }
    
    func request<T: Model>(request: Target, model: T.Type) -> AnyPublisher<T, Error> {
        //        requestWithoutaaaMapping(request: request, model: model)
        return requestWithoutMapping(request: request)
            .catchCommonErrors()
            .tryMap({ response in
                if let error = self.resutlStatus(response.statusCode) {
                    throw error
                }
                return try JSONDecoder().decode(T.self, from: response.data)
            })
            .eraseToAnyPublisher()
    }
    
    func requestArr<T: Model>(request: Target, model: T.Type) -> AnyPublisher<T, Error> {
        
        requestWithoutMapping(request: request)
            .catchCommonErrors()
            .tryMap { response -> T in
                if let error = self.resutlStatus(response.statusCode) {
                    throw error
                }
                guard var convertedString = String(data: response.data, encoding: String.Encoding.utf8) else {
                    return model.empty()
                }
                if convertedString.prefix(1) == "[" {
                    convertedString = "{ \"data\": " + convertedString + "}"
                }
                guard let data = convertedString.data(using: .utf8) else { return model.empty() }
                return try JSONDecoder().decode(T.self, from: data)
            }
            .eraseToAnyPublisher()
    }
    
    func requestWithoutMapping(request: Target) -> AnyPublisher<Response, MoyaError> {
        return requestPublisher(request)
            .retry(1)
            .first()
            .eraseToAnyPublisher()
    }
    
    func requestWithoutaaaMapping(request: Target,  model: Model.Type) {
        requestPublisher(request)
            .retry(1)
            .first()
            .sink(receiveCompletion: { error in
                print(error)
            }, receiveValue: { response in
                let decoder = JSONDecoder()
                do {
                    let a = try decoder.decode(model.self, from: response.data)
                } catch let error {
                    print(error)
                }
                
            })
            .store(in: &cancelables)
    }
    
    internal func resutlStatus(_ statusCode: Int) -> CommonErrors? {
        if statusCode >= 500 || statusCode == 403 || statusCode == 404 {
            return .serverNotResponding
        }
        return nil
    }
}

public enum CommonErrors: LocalizedError {
    case unownedStatusCode
    case notLoggedIn
    case objectMappingFailed(description: String)
    case wrongParameterSentOnServer(description: String)
    case serverNotResponding

    public var errorDescription: String? {
        switch self {
        case .notLoggedIn:
            return "You should log in if you want play something."
        case .objectMappingFailed(description: let message):
            return "Parsing of the received data failed. Please contact the admin. Message: \(message)"
        case .wrongParameterSentOnServer(description: let message):
            return "The admin setups wrong parameters for reqeust on server. Please contact the admin. Message: \(message)"
        case .unownedStatusCode:
            return "Unowned HTTP Status Code"
        case .serverNotResponding:
            return "Server is not responding, please try agin later"
        }
    }

}

extension AnyPublisher where Failure == MoyaError {

    public func catchCommonErrors() -> AnyPublisher<Output, Error> {
        return  self.catch { (mError) -> Fail<Output, Error> in
            /// Indicates a response failed to map to a Decodable object.
            if case .statusCode(let response) = mError, response.statusCode == 401 {
                return Fail(error: CommonErrors.notLoggedIn)
            } else if case .objectMapping(_, let response) = mError {
                return Fail(error: CommonErrors.objectMappingFailed(description: response.description))
            } else if case .parameterEncoding(let response) = mError {
                return Fail(error: CommonErrors.wrongParameterSentOnServer(description: response.localizedDescription))
            }
            // Do additinal handling here if needed
            return Fail(error: mError)
        }
        .eraseToAnyPublisher()
    }
}

extension Publisher where Output == Response, Failure == Error  {


    /// Maps received data at key path into a Decodable object. If the conversion fails, the signal errors.
    func map<D: Decodable>(_ type: D.Type, atKeyPath keyPath: String? = nil, using decoder: JSONDecoder = JSONDecoder(), failsOnEmptyData: Bool = true) -> AnyPublisher<D, Error> {
        return unwrapThrowable { response in
            try response.map(type, atKeyPath: keyPath, using: decoder, failsOnEmptyData: failsOnEmptyData)
        }
    }
}

extension Publisher where Failure == Error {

    // Workaround for a lot of things, actually. We don't have Publishers.Once, flatMap
    // that can throw and a lot more. So this monster was created because of that. Sorry.
    private func unwrapThrowable<T>(throwable: @escaping (Output) throws -> T) -> AnyPublisher<T, Error> {
        self.tryMap { element in
            try throwable(element)
        }
        .mapError { error -> MoyaError in
            if let moyaError = error as? MoyaError {
                return moyaError
            } else {
                return .underlying(error, nil)
            }
        }
        .eraseToAnyPublisher()
    }
}

import UIKit
public enum RequestErrors: Error {
    case requestFailed
    case httpError
    case decodeError
    case noMovies
    case noDataForRequest
    case unauthorized

    func handleError(on: UIViewController?) {
        if on == nil {
            return
        }
        let alert = UIAlertController(title: String(localized: .error), message: self.localizedDesc, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: String(localized: .buttonOK), style: .cancel, handler: nil))
        DispatchQueue.main.async {
            on?.present(alert, animated: true, completion: nil)
        }
    }
    
    private var localizedDesc:String {
        get {
            switch self {
            case .unauthorized:
                return String(localized: .unauthorized)
            case .decodeError:
                return String(localized: .decodeError)
            case .httpError:
                return String(localized: .httpError)
            case .noDataForRequest:
                return String(localized: .noDataForRequest)
            case .noMovies:
                return String(localized: .noMovies)
            case .requestFailed:
                return String(localized: .requestFailed)
            }
        }
    }
}
