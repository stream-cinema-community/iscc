//
//  LoginManager.swift
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import CommonCrypto
import Moya
import Combine
import KeychainStorage
import TraktKit
import CryptoKit

enum WsSeecret {
    case name
    case pass
    case token
    case passHash
    case hashedName
    case usersHashs
    
    var secreet: String {
        switch self {
        case .name:
            return "com.scc.atv.ws.name"
        case .pass:
            return "com.scc.atv.ws.pass"
        case .token:
            return "com.scc.atv.ws.token"
        case .passHash:
            return "com.scc.atv.ws.passHash"
        case .hashedName:
            return "com.scc.atv.ws.hashedName"
        case .usersHashs:
            return "com.scc.atv.ws.usersHashs"
        }
    }
}

enum OSSeecret {
    case name
    case pass
    case token
    
    var secreet: String {
        switch self {
        case .name:
            return "com.scc.atv.os.name"
        case .pass:
            return "com.scc.atv.os.pass"
        case .token:
            return "com.scc.atv.os.token"
        }
    }
}

extension LoginManager {

    public static var osTokenHash:String? {
        get {
            if let string = try? LoginManager.storage.string(forKey: OSSeecret.token.secreet) {
                return "Bearer " + string
            }
            return nil
        }
    }
    public static var osName:String? {
           get {
               return try? LoginManager.storage.string(forKey: OSSeecret.name.secreet)
           }
       }
       
    public static var osPass:String? {
        get {
            return try? LoginManager.storage.string(forKey: OSSeecret.name.secreet)
        }
    }
    
    func osLogin(name:String? = nil, pass:String? = nil) -> AnyPublisher<OSLoginResult, Error> {
        
        guard let userName = name ?? LoginManager.osName,
            let password = pass ?? LoginManager.pass else {
            return Fail(error: LoginManagerError.unknownUserLoginCredentials).eraseToAnyPublisher()
        }

        return osService.login(name: userName, pass: password)
            .handleEvents(receiveOutput: { data in
                try? LoginManager.storage.set(userName, key: OSSeecret.name.secreet)
                try? LoginManager.storage.set(password, key: OSSeecret.pass.secreet)
                if let token = data.token {
                    try? LoginManager.storage.set(token, key: OSSeecret.token.secreet)
                }
            })
            .eraseToAnyPublisher()
    }
}

public final class LoginManager {

    enum LoginManagerError: LocalizedError {
        case unknownUserLoginCredentials
        case unknownLoginToken
        case missingSaltOrFailedCreatingOfHash

        var errorDescription: String? {
            switch self {
            case .unknownUserLoginCredentials: return "We dont have your credentials to login."
            case .unknownLoginToken: return "We miss your login token"
            case .missingSaltOrFailedCreatingOfHash: return "Some problems with password"
            }
        }
    }

    private static let storage:KeychainStorage = KeychainStorage(service: "com.scc.atv")
    
    public static var tokenHash:String? {
        get {
        #if targetEnvironment(simulator)
            return UserDefaults.standard.string(forKey: WsSeecret.token.secreet)
        #else
            return try? LoginManager.storage.string(forKey: WsSeecret.token.secreet)
        #endif
        }
        set {
        #if targetEnvironment(simulator)
            UserDefaults.standard.set(newValue, forKey: WsSeecret.token.secreet)
        #else
            if let newValue {
                try? LoginManager.storage.set(newValue, key: WsSeecret.token.secreet)
            } else {
                try? LoginManager.storage.removeValue(forKey: WsSeecret.token.secreet)
            }
        #endif
        }
    }
    
    public static var myNameHash: String? {
        get {
        #if targetEnvironment(simulator)
            return UserDefaults.standard.string(forKey: WsSeecret.hashedName.secreet)
        #else
            return try? LoginManager.storage.string(forKey: WsSeecret.hashedName.secreet)
        #endif
        }
        set {
        #if targetEnvironment(simulator)
            UserDefaults.standard.set(newValue, forKey: WsSeecret.hashedName.secreet)
        #else
            if let newValue {
                try? LoginManager.storage.set(newValue, key: WsSeecret.hashedName.secreet)
            } else {
                try? LoginManager.storage.removeValue(forKey: WsSeecret.hashedName.secreet)
            }
        #endif
        }
    }
    
    public static var passwordHash:String? {
        get {
        #if targetEnvironment(simulator)
            return UserDefaults.standard.string(forKey: WsSeecret.passHash.secreet)
        #else
            return try? LoginManager.storage.string(forKey: WsSeecret.passHash.secreet)
        #endif
        }
        set {
        #if targetEnvironment(simulator)
            UserDefaults.standard.set(newValue, forKey: WsSeecret.passHash.secreet)
        #else
            if let newValue {
                try? LoginManager.storage.set(newValue, key: WsSeecret.passHash.secreet)
            } else {
                try? LoginManager.storage.removeValue(forKey: WsSeecret.passHash.secreet)
            }
        #endif
        }
    }
    
    public static var name:String? {
        get {
        #if targetEnvironment(simulator)
            return UserDefaults.standard.string(forKey: WsSeecret.name.secreet)
        #else
            return try? LoginManager.storage.string(forKey: WsSeecret.name.secreet)
        #endif
        }
        set {
        #if targetEnvironment(simulator)
            UserDefaults.standard.set(newValue, forKey: WsSeecret.name.secreet)
        #else
            if let newValue {
                try? LoginManager.storage.set(newValue, key: WsSeecret.name.secreet)
            } else {
                try? LoginManager.storage.removeValue(forKey: WsSeecret.name.secreet)
            }
        #endif
        }
    }
    
    public static var pass:String? {
        get {
        #if targetEnvironment(simulator)
            return UserDefaults.standard.string(forKey: WsSeecret.pass.secreet)
        #else
            return try? LoginManager.storage.string(forKey: WsSeecret.pass.secreet)
        #endif
        }
        set {
        #if targetEnvironment(simulator)
            UserDefaults.standard.set(newValue, forKey: WsSeecret.pass.secreet)
        #else
            if let newValue {
                try? LoginManager.storage.set(newValue, key: WsSeecret.pass.secreet)
            } else {
                try? LoginManager.storage.removeValue(forKey: WsSeecret.pass.secreet)
            }
        #endif
        }
    }
    
    public static var usersHashs: [String:String] {
        get {
            var data: Data?
            #if targetEnvironment(simulator)
            data = UserDefaults.standard.data(forKey: WsSeecret.usersHashs.secreet)
            #else
            data = try? LoginManager.storage.data(forKey: WsSeecret.usersHashs.secreet)
            #endif
            if let data, let dict = try? JSONDecoder().decode([String:String].self, from: data) {
                return dict
            }
            return [:]
        }
        set {
            guard let jsonData = try? JSONEncoder().encode(newValue) else { return }
            #if targetEnvironment(simulator)
                UserDefaults.standard.set(jsonData, forKey: WsSeecret.pass.secreet)
            #else
            try? LoginManager.storage.set(jsonData, key: WsSeecret.pass.secreet)
            #endif
        }
    }


    fileprivate let wsService: WSService
    fileprivate let osService: OSService

    init(wsService: WSService, osService: OSService) {
        self.osService = osService
        self.wsService = wsService
    }

    
    func connectWithExistingData() -> AnyPublisher<Bool, Error> {
        guard let name = LoginManager.name,
              let pass = LoginManager.pass else {
            return Fail(error: LoginManagerError.unknownUserLoginCredentials).eraseToAnyPublisher()
        }
        return connect(name: name, pass: pass)
    }
    
    func connect(name: String?, pass: String?, isForceLogin: Bool = false) -> AnyPublisher<Bool, Error> {
        if self.isLoggedOn(), isForceLogin == false {
            return Just(true).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
        guard  let name = name ?? LoginManager.name,
               let pass = pass ?? LoginManager.pass else {
            return Fail(error: LoginManagerError.unknownUserLoginCredentials).eraseToAnyPublisher()
        }
        return getSalt(name: name)
            .flatMap {
                self.login(with: name, password: pass, model: $0)
            }
            .eraseToAnyPublisher()
    }

    
    private func isLoggedOn() -> Bool {
        if let token = LoginManager.tokenHash,
            LoginManager.name != LoginManager.pass,
            let delegate = UIApplication.shared.delegate as? AppDelegate {
            let tokenData = WSTokenData(token: token, passwordHash: LoginManager.passwordHash, name: LoginManager.name)
            delegate.appData.token = tokenData
            return true
        }
        return false
    }
    
    func logout(isEraseAllData: Bool) -> AnyPublisher<Bool, Error> {
        guard let token = LoginManager.tokenHash,
              let uuid = UIDevice.current.identifierForVendor else {
                return Fail(error: LoginManagerError.unknownLoginToken).eraseToAnyPublisher()
        }
        return wsService.logout(device_uuid: uuid, token: token)
            .handleEvents(receiveOutput: { _ in
                LoginManager.removeAppCredentials()
                if isEraseAllData {
                    LoginManager.eraseAllData()
                    TraktManager.sharedManager.signOut()
                }
            })
            .eraseToAnyPublisher()
    }
    
    private static func eraseAllData() {
        WatchedWrapper.deleteAllData(for: .scc)
        WatchedWrapper.deleteAllData(for: .trakt)
        TraktManager.sharedManager.signOut()
        CurrentAppSettings.passCode = nil
    }
    
    private static func removeAppCredentials() {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate  else { return }
        delegate.appData.token = nil
        LoginManager.pass = nil
        LoginManager.name = nil
        LoginManager.myNameHash = nil
        LoginManager.tokenHash = nil
    }
    
     
    
    private func login(with name:String, password: String, model: SaltModel) -> AnyPublisher<Bool,Error> {
        if let salt = model.salt,
            let passwordHash = getHash(password, salt: salt),
            let delegate = UIApplication.shared.delegate as? AppDelegate {
            let data:[String : Any] = ["username_or_email":name, "password":passwordHash, "keep_logged_in":1]
            LoginManager.myNameHash = sha256(name: name, salt: salt)

            return wsService.login(params: data)
                .handleEvents(receiveOutput: { [weak delegate] model in
                    LoginManager.name = name
                    LoginManager.pass = password
                    LoginManager.passwordHash = passwordHash
                    if let token = model.token {
                        LoginManager.tokenHash = token
                    }
                    let tokenData = WSTokenData(token: model.token, passwordHash: passwordHash, name: name)
                    delegate?.appData.token = tokenData
                }, receiveCompletion: { [weak delegate] (completion) in
                    guard case .failure(_) = completion else { return }
                    delegate?.appData.token = nil
                })
                .map { _ in return true }
                .eraseToAnyPublisher()
        } else {
            return Fail(error: LoginManagerError.missingSaltOrFailedCreatingOfHash).eraseToAnyPublisher()
        }
    }
    
    func addFriend(user: String?) async throws {
        guard let user,
              let salt = try await getSaltAsync(name: user).salt
        else { return }
        
        let userHash = sha256(name: user, salt: salt)
        
        var allHashs = LoginManager.usersHashs
        allHashs[user] = userHash
        
        LoginManager.usersHashs = allHashs
    }
    
    func removeFriend(user: String) {
        var allHashs = LoginManager.usersHashs
        allHashs.removeValue(forKey: user)
        LoginManager.usersHashs = allHashs
    }

    func getSalt(name: String) -> AnyPublisher<SaltModel, Error> {
        wsService.getSalt(salt: name)
    }
    
    func getSaltAsync(name: String) async throws -> SaltModel {
        try await wsService.getSaltAsync(salt: name)
    }
    
    public func sha256(name: String, salt: String) -> String {
        let inputString = name + salt
        let inputData = Data(inputString.utf8)
        let hashed = SHA256.hash(data: inputData)
       
        return hashed.compactMap { String(format: "%02x", $0) }.joined()
    }
    
    func myNameHashAsync() async throws -> String? {
        guard let name = LoginManager.name else { return nil }
        let saltModel = try await getSaltAsync(name: name)
        guard let salt = saltModel.salt else { return nil }
        let hashedName = sha256(name: name, salt: salt)
        LoginManager.myNameHash = hashedName
        return hashedName
    }
    
    private func getHash(_ password: String, salt: String, prefix:String = "$1$") -> String? {
        if let md5Hash = HashPass.md5Crypt(password, salt: salt, prefix: prefix) {
            return md5Hash.sha1()
        }
        return nil
    }
}

extension String {
    func sha1() -> String {
        let data = Data(self.utf8)
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0.baseAddress, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}
