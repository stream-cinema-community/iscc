//
//  TvModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 25/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

struct StationEPG: Codable, Model {
    let currnet, next: Program?
    
    enum CodingKeys: String, CodingKey {
        case currnet = "currentProgram"
        case next = "nextProgram"
    }
    
    static func empty<T>() -> T where T : Model {
        let tv = StationEPG(currnet: nil, next: nil)
        guard let emptyTV = tv as?T else {
            fatalError()
        }
        return emptyTV
    }
}

struct Program: Codable {
    let programName, desc, episode, startAt: String?
    let stopAt, channel: String?
    
    func getStart() -> Date? {
        guard let startAt = self.startAt else { return nil }
        if let date = self.format(string: startAt) {
            return date
        }
        return Date()
    }
    
    func getEnd() -> Date? {
        guard let stopAt = self.stopAt else { return nil }
        if let date = self.format(string: stopAt) {
            return date
        }
        return Date()
    }
    
    private func format(string:String) -> Date? {
        let dateFormatter = DateFormatter() //2020-12-09 20:00:00
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:s"
        return dateFormatter.date(from: string)
    }
    
    enum CodingKeys: String, CodingKey {
        case programName
        case desc
        case episode
        case startAt
        case stopAt
        case channel
    }
    
    static func empty<T>() -> T where T : Model {
        let tv = Program(programName: nil, desc: nil, episode: nil, startAt: nil, stopAt: nil, channel: nil)
        guard let emptyTV = tv as?T else {
            fatalError()
        }
        return emptyTV
    }
}

struct TVs: Model {
    let data:[TV]
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    static func empty<T>() -> T where T : Model {
        let tvs = TVs(data: [])
        guard let emptyTVs = tvs as?T else {
            fatalError()
        }
        return emptyTVs
    }
    
    public var tvs:[TV] {
        get {
            return self.data.filter { tv -> Bool in
                return tv.url != nil
            }
        }
    }
}

struct TV: Codable, Model {
    let id: Int?
    let tvtitle: String?
    let url: String?
    let extFilter: String?
    let group, tvid: String?
    let tvlogo: String?
    var epg: StationEPG?
    var isReloaded:Bool = false

    enum CodingKeys: String, CodingKey {
        case id, tvtitle, url
        case extFilter
        case group, tvid, tvlogo
    }
    
    static func empty<T>() -> T where T : Model {
        let tv = TV(id: nil, tvtitle: nil, url: nil, extFilter: nil, group: nil, tvid: nil, tvlogo: nil, epg: nil)
        guard let emptyTV = tv as?T else {
            fatalError()
        }
        return emptyTV
    }
}
