//
//  FriendsShareModel.swift
//  StreamCinema.atv
//
//  Created by iSCC on 10/02/2023.
//  Copyright © 2023 SCC. All rights reserved.
//

import Foundation
// MARK: - WelcomeElement
struct FriendsShareModel: Codable {
    let id: Int
    let name, sccID: String
    let added: String
    let isPositive, isMovie: Int
    var isMe: Bool = false
    
    var dateAdded: Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: added)
    }
    
    var isRecommend: Bool {
        isPositive == 1
    }
    
    var isTVShow: Bool {
        isMovie == 0
    }
    
    enum CodingKeys: CodingKey {
        case id
        case name
        case sccID
        case added
        case isPositive
        case isMovie
    }
}

typealias FriendsShareModels = [FriendsShareModel]
