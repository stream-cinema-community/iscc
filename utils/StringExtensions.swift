//
//  StringExtensions.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import iSCCCore

extension String {

    var asURL: URL? {
        return URL(string: self)
    }
    
    var toDate: Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: self)
    }

    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }

    init(localized text: Resource.Text) {
        self.init(text.localized)
    }

    /// Localization with key, but we should not use it any more. All items should be defined in iSCCCore.Reource.Text.
    init(localizationKey: String) {
        self.init(localizationKey.localized)
    }

}

