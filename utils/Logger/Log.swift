//
//  Log.swift
//  StreamCinema.atv
//
//  Created by SCC on 23/02/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation

final class Log: NSObject {
    
    static let shared = Log()
    
    var file: URL = Log.logFileName()
    var fileHandle: FileHandle?
    var queue: DispatchQueue = DispatchQueue(label: "Logger")
    var formatter: DateFormatter?
    var isDebug: Bool {
        get {
            #if DEBUG
                return true
            #else
                return false
            #endif
        }
    }
    
    public class func write(_ text: String) {
        return
        if Log.shared.isDebug {
            Log.shared.queue.sync(execute: {
                if let file = Log.getFileHandle() {
                    let time = Log.getTime()
                    let printed = time + " " + text + "\n"
                    if let data = printed.data(using: String.Encoding.utf8) {
                        file.seekToEndOfFile()
                        file.write(data)
                    }
                }
            })
            print("iSCC_Log: \(text)")
        }
    }
    
    public class func clearOldLogs() {
        do {
            let fileManager = FileManager.default
            let tmpDirectory = FileManager.default.temporaryDirectory
            let tempContent = try fileManager.contentsOfDirectory(atPath: tmpDirectory.path)
            let compareDate = Date.init(timeIntervalSinceNow: -86400)
            try tempContent.forEach { file in
                guard file.contains("iSCC_Log_") else { return }
                
                let fileUrl = tmpDirectory.appendingPathComponent(file)
                let attrs = try fileManager.attributesOfItem(atPath: fileUrl.path) as NSDictionary
                guard let date = attrs.fileCreationDate() else { return }
                
                if date < compareDate {
                    try fileManager.removeItem(atPath: fileUrl.path)
                }
            }
            Log.write("clearOldLogs success")
        } catch {
            Log.write(error.localizedDescription)
        }
    }
    
    private static func getFileHandle() -> FileHandle? {
        if Log.shared.fileHandle == nil {
            let fileManager = FileManager.default
            if !fileManager.fileExists(atPath: Log.shared.file.absoluteString) {
                fileManager.createFile(atPath: Log.shared.file.absoluteString, contents: nil, attributes: nil)
            }
            
            do {
                Log.shared.fileHandle = try FileHandle(forWritingTo: Log.shared.file)
            } catch let error {
                print("Unexpected error writing to log: : \(error).")
            }
        }
        
        return Log.shared.fileHandle
    }
    
    private static func getTime() -> String {
        if Log.shared.formatter == nil {
            Log.shared.formatter = DateFormatter()
            Log.shared.formatter!.dateFormat = "y-M-d HH:mm:ss.SSSSZ"
        }
        return Log.shared.formatter!.string(from: Date())
    }
    
    class func logFileName() -> URL {
        var url = FileManager.default.temporaryDirectory
        
        let formater = DateFormatter()
        formater.dateFormat = "'iSCC_Log_'dd_MM_yyyy'.txt'"
        let fileName = formater.string(from: Date())
        url.appendPathComponent(fileName)
        
        let isExists = FileManager.default.fileExists(atPath: url.path)
        
        if !isExists {
            FileManager.default.createFile(atPath: url.path, contents: nil, attributes: nil)
        }
        
        return url
    }
}
