//
//  KSPlayer+SCCPlayer.swift
//  StreamCinema.atv
//
//  Created by iSCC on 28/02/2023.
//  Copyright © 2023 SCC. All rights reserved.
//

import Foundation
import KSPlayer

final class SCCPlayerKS: UIView {
    weak var delegate: SCCPlayerDelegate?
    
    internal var player: VideoPlayerView
    internal var media: PlayerMedia?
    
    init() {
        player = VideoPlayerView(frame: .zero)
        super.init(frame: .zero)
        player.delegate = self
        configureViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var mediaPlayer: MediaPlayerProtocol? {
        player.playerLayer?.player
    }
    
    internal var state: KSPlayerState? {
        player.playerLayer?.state
    }
    
    internal func configureViews() {
        addSubview(player)
        
        player.translatesAutoresizingMaskIntoConstraints = false
        
        player.topAnchor.constraint(equalTo: topAnchor).isActive = true
        player.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        player.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        player.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    var audioTracks: [Track] {
        mediaPlayer?.tracks(mediaType: .audio).compactMap({ Track(title: $0.name,
                                                                  id: "\($0.trackID)",
                                                                  isSelected: $0.isEnabled,
                                                                  type: .audio) }) ?? []
    }
    
    var sttTracks: [Track] {
        player.srtControl.filterInfos({ _ in true }).compactMap({ Track(title: $0.name,
                                                                        id: $0.subtitleID,
                                                                        isSelected: $0.subtitleID == player.srtControl.view.selectedInfo?.subtitleID,
                                                                        type: .srt) })
    }
}

extension SCCPlayerKS: SCCPlayer {
    func select(track: Track) {
        switch track.type {
        case .audio:
            guard let audio = mediaPlayer?.tracks(mediaType: .audio).first(where: { "\($0.trackID)" == track.id }) else { return }
            mediaPlayer?.select(track: audio)
        case .srt:
            let srt = player.srtControl.filterInfos({ _ in true }).first(where: { $0.subtitleID == track.id })
            player.srtControl.view.selectedInfo = srt
        }
    }
    
    func set(media: PlayerMedia) {
        self.media = media
        let options = KSOptions()
        options.isAutoPlay = true
        KSOptions.enableVolumeGestures = false
        KSOptions.enablePlaytimeGestures = false
        KSOptions.enableBrightnessGestures = false
        KSOptions.topBarShowInCase = .none
        KSOptions.firstPlayerType = KSMEPlayer.self
        player.set(url: media.playableURL, options: options)
    }
    
    func seek(to time: TimeInterval, completion: ((Bool) -> Void)?) {
        mediaPlayer?.seek(time: time) { finished in
            completion?(finished)
        }
    }
    
    func play(at time: TimeInterval?, completion: ((Bool) -> Void)?) {
        if let time {
            mediaPlayer?.seek(time: time) { finished in
                completion?(finished)
            }
        } else {
            player.play()
            completion?(true)
        }
    }
    
    func stop() {
        player.resetPlayer()
    }
    
    func forward() {
        guard let state, state.isPlaying, player.toolBar.isSeekable else { return }
        seek(to: player.toolBar.currentTime + 15) { _ in }
    }
    
    func backward() {
        guard let state, state.isPlaying, player.toolBar.isSeekable else { return }
        seek(to: player.toolBar.currentTime + 15) { _ in }
    }
}

extension SCCPlayerKS: PlayerControllerDelegate {
    func playerController(state: KSPlayer.KSPlayerState) {
        let state = SCCPlayerState.state(for: state, with: nil)
        delegate?.player(self, state: state)
    }
    
    func playerController(currentTime: TimeInterval, totalTime: TimeInterval) {
        delegate?.player(self, currentTime: currentTime, totalTime: totalTime)
    }
    
    func playerController(finish error: Error?) {
        if let error {
            let state = SCCPlayerState.state(for: .error, with: error)
            delegate?.player(self, state: state)
        }
        delegate?.player(self, finish: error)
    }
    
    func playerController(maskShow: Bool) { }
    
    func playerController(action: KSPlayer.PlayerButtonType) { }
    
    func playerController(bufferedCount: Int, consumeTime: TimeInterval) {
        delegate?.player(self, bufferedCount: bufferedCount, consumeTime: consumeTime)
    }
}


extension SCCPlayerState {
    static func state(for state: KSPlayer.KSPlayerState, with error: Error?) -> SCCPlayerState {
        switch state {
        case .prepareToPlay:
            return .prepareToPlay
        case .readyToPlay:
            return .readyToPlay
        case .buffering:
            return .buffering
        case .bufferFinished:
            return .bufferFinished
        case .paused:
            return .paused
        case .playedToTheEnd:
            return .playedToTheEnd
        case .error:
            return .error(error: error)
        }
    }
}
