//
//  SCCPlayerToolBar.swift
//  StreamCinema.atv
//
//  Created by iSCC on 28/02/2023.
//  Copyright © 2023 SCC. All rights reserved.
//

import UIKit

protocol SCCPlayerToolBarDelegate: AnyObject {
    func toolBar(_ toolBar: SCCPlayerToolBar, didSelect track: Track)
    func toolBar(_ toolBar: SCCPlayerToolBar, tracksFor type: TrackType) -> [Track]
}

final class SCCPlayerToolBar: UIView {
    private let progressBar = UIProgressView()
    private let currentTimeLabel = UILabel()
    private let elapsedTimeLabel = UILabel()
    private let seekTimeLabel = UILabel()
    private let titleLabel = UILabel()
    private let subTitleLabel = UILabel()
    private let acionMenu = UIStackView()
    
    weak var delegate: SCCPlayerToolBarDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var currentTime: TimeInterval = 0 {
        didSet {
            guard currentTime > 0, totalTime >= 0 else  { return }
            let text = currentTime.toString(for: .minOrHour)
            currentTimeLabel.text = text
            progressBar.progress = Float(currentTime)/Float(totalTime)
        }
    }
    
    var totalTime: TimeInterval = 0 {
        didSet {
            guard totalTime > 0 else  { return }
            let text = (totalTime - currentTime).toString(for: .minOrHour)
            elapsedTimeLabel.text = "- " + text
        }
    }
    
    var isBuffering: Bool = false {
        didSet {
            
        }
    }
    
    var buffferCount: Int = 0 {
        didSet {
            
        }
    }
    
    var palybackSpeeds: [Double] = [] {
        didSet {
            
        }
    }
    
    var title: String? {
        get {
            titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }
    
    var subTitle: String? {
        get {
            subTitleLabel.text
        }
        set {
            subTitleLabel.text = newValue
        }
    }
}

internal extension SCCPlayerToolBar {
    func configureViews() {
        addSubview(progressBar)
        addSubview(currentTimeLabel)
        addSubview(elapsedTimeLabel)
        addSubview(seekTimeLabel)
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        addSubview(acionMenu)
        
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        currentTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        elapsedTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        seekTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        acionMenu.translatesAutoresizingMaskIntoConstraints = false
        
        
        currentTimeLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        currentTimeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -36).isActive = true
        currentTimeLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        
        elapsedTimeLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        elapsedTimeLabel.centerYAnchor.constraint(equalTo: currentTimeLabel.centerYAnchor).isActive = true
        elapsedTimeLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        
        progressBar.heightAnchor.constraint(equalToConstant: 10).isActive = true
        progressBar.rightAnchor.constraint(equalTo: elapsedTimeLabel.leftAnchor, constant: -16).isActive = true
        progressBar.leftAnchor.constraint(equalTo: currentTimeLabel.rightAnchor, constant: 16).isActive = true
        progressBar.centerYAnchor.constraint(equalTo: currentTimeLabel.centerYAnchor).isActive = true
        
        
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 50).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: subTitleLabel.topAnchor, constant: 16).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        subTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 50).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: progressBar.topAnchor, constant: 32).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        
        currentTimeLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 24, weight: .regular)
        currentTimeLabel.textAlignment = .right
        elapsedTimeLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 24, weight: .regular)
        elapsedTimeLabel.textAlignment = .left
    }
}
