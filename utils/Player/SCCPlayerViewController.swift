//
//  SCCPlayerViewController.swift
//  StreamCinema.atv
//
//  Created by iSCC on 28/02/2023.
//  Copyright © 2023 SCC. All rights reserved.
//

import Foundation

protocol SCCPlayerViewControllerDelegate: AnyObject {
    func sccPlayer(_ controller:SCCPlayerViewController, videoAfter media: PlayerMedia) -> PlayerMedia?
    func sccPlayer(_ controller:SCCPlayerViewController, currentTime: TimeInterval, totalTime: TimeInterval)
}

final class SCCPlayerViewController: UIViewController {
    let player: SCCPlayer
    let toolBar: SCCPlayerToolBar = SCCPlayerToolBar(frame: .zero)
    
    weak var delegate: SCCPlayerViewControllerDelegate?
    var onCloseWithError: ((Error) -> Void)?
    
    init(nibName nibNameOrNil: String? = nil, bundle nibBundleOrNil: Bundle? = nil, type: SCCPlayerType = .vlc) {
        player = SCCPlayerKS()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        player.delegate = self
        toolBar.delegate = self
        configureViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func configureViews() {
        view.addSubview(player)
        view.addSubview(toolBar)
        
        player.translatesAutoresizingMaskIntoConstraints = false
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        
        player.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        player.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        player.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        player.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        toolBar.heightAnchor.constraint(equalToConstant: 120).isActive = true
        toolBar.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        toolBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        toolBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    func play(_ media:PlayerMedia) {
        player.set(media: media)
        toolBar.title = media.title
        toolBar.subTitle = media.subtitle
    }
}

extension SCCPlayerViewController {
    func configureAudio() {
        
    }
    
    func showToolBar() {
        
    }
    
    func hideToolBar() {
        
    }
    
    func playNextOrClose() {
        if let currentMedia = player.media,
           let media = delegate?.sccPlayer(self, videoAfter: currentMedia) {
            player.set(media: media)
        } else {
            close()
        }
    }
    
    func close() {
        dismiss(animated: true)
    }
}

extension SCCPlayerViewController: SCCPlayerToolBarDelegate {
    func toolBar(_ toolBar: SCCPlayerToolBar, tracksFor type: TrackType) -> [Track] {
        switch type {
        case .audio:
            return player.audioTracks
        case .srt:
            return player.sttTracks
        }
    }
    
    func toolBar(_ toolBar: SCCPlayerToolBar, didSelect track: Track) {
        player.select(track: track)
    }
}

extension SCCPlayerViewController: SCCPlayerDelegate {
    func player(_ player: SCCPlayer, state: SCCPlayerState) {
        toolBar.isBuffering = (state == .buffering)
        
        switch state {
        case .prepareToPlay:
            break
        case .readyToPlay:
            configureAudio()
        case .buffering:
            showToolBar()
        case .bufferFinished:
            hideToolBar()
        case .paused:
            showToolBar()
        case .playedToTheEnd:
            playNextOrClose()
        case .error(let error):
            if let error {
                onCloseWithError?(error)
            } else {
                close()
            }
        }
    }
    
    func player(_ player: SCCPlayer, currentTime: TimeInterval, totalTime: TimeInterval) {
        toolBar.totalTime = totalTime
        toolBar.currentTime = currentTime
        delegate?.sccPlayer(self, currentTime: currentTime, totalTime: totalTime)
    }
    
    func player(_ player: SCCPlayer, finish error: Error?) {
        
    }
    
    func player(_ player: SCCPlayer, bufferedCount: Int, consumeTime: TimeInterval) {
        toolBar.buffferCount = bufferedCount
    }
}
