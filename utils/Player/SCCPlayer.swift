//
//  SCCPlayer.swift
//  StreamCinema.atv
//
//  Created by iSCC on 28/02/2023.
//  Copyright © 2023 SCC. All rights reserved.
//

import Foundation

enum SCCPlayerType {
    case vlc
    case ksPlayer
}

public enum SCCPlayerState: Equatable  {
    case prepareToPlay
    case readyToPlay
    case buffering
    case bufferFinished
    case paused
    case playedToTheEnd
    case error(error: Error?)
    
    public var description: String {
        switch self {
        case .prepareToPlay:
            return "prepareToPlay"
        case .readyToPlay:
            return "readyToPlay"
        case .buffering:
            return "buffering"
        case .bufferFinished:
            return "bufferFinished"
        case .paused:
            return "paused"
        case .playedToTheEnd:
            return "playedToTheEnd"
        case .error(let error):
            return "error \(String(describing: error?.localizedDescription))"
        }
    }
    
    public static func == (lhs: SCCPlayerState, rhs: SCCPlayerState) -> Bool {
        lhs.description == rhs.description
    }
}

protocol SCCPlayerDelegate: AnyObject {
    func player(_ player: SCCPlayer, state: SCCPlayerState)
    func player(_ player: SCCPlayer, currentTime: TimeInterval, totalTime: TimeInterval)
    func player(_ player: SCCPlayer, finish error: Error?)
    func player(_ player: SCCPlayer, bufferedCount: Int, consumeTime: TimeInterval)
}

protocol SCCPlayer: UIView {
    var delegate: SCCPlayerDelegate? { get set }
    
    var audioTracks: [Track] { get }
    var sttTracks: [Track] { get }
    var media: PlayerMedia? { get }
    
    func select(track: Track)
    func set(media: PlayerMedia)
    func seek(to time: TimeInterval, completion: ((Bool) -> Void)?)
    func play(at time: TimeInterval?, completion: ((Bool) -> Void)?)
    func stop()
    func forward()
    func backward()
}

enum TrackType {
    case audio
    case srt
}

struct Track {
    let title: String
    let id: String
    let isSelected: Bool
    let type: TrackType
}

struct PlayerMedia {
    var playableURL: URL
    var title: String
    var subtitle: String?
    var desc: String?
    var nowPlayingInfo = [String: Any]()
}


//let MPNowPlayingInfoCollectionIdentifier: String
//The identifier of the collection the Now Playing item belongs to.
//let MPNowPlayingInfoPropertyAdTimeRanges: String
//A list of ad breaks in the Now Playing item.
//let MPNowPlayingInfoPropertyAvailableLanguageOptions: String
//The available language option groups for the Now Playing item.
//let MPNowPlayingInfoPropertyAssetURL: String
//The URL pointing to the Now Playing item’s underlying asset.
//let MPNowPlayingInfoPropertyChapterCount: String
//The total number of chapters in the Now Playing item.
//let MPNowPlayingInfoPropertyChapterNumber: String
//The number corresponding to the currently playing chapter.
//let MPNowPlayingInfoPropertyCreditsStartTime: String
//The start time for the credits, in seconds, without ads, for the Now Playing item.
//let MPNowPlayingInfoPropertyCurrentLanguageOptions: String
//The currently active language options for the Now Playing item.
//let MPNowPlayingInfoPropertyCurrentPlaybackDate: String
//The date associated with the current elapsed playback time.
//let MPNowPlayingInfoPropertyDefaultPlaybackRate: String
//The default playback rate for the Now Playing item.
//let MPNowPlayingInfoPropertyElapsedPlaybackTime: String
//The elapsed time of the Now Playing item, in seconds.
//let MPNowPlayingInfoPropertyExternalContentIdentifier: String
//The opaque identifier that uniquely identifies the Now Playing item, even through app relaunches.
//let MPNowPlayingInfoPropertyExternalUserProfileIdentifier: String
//The opaque identifier that uniquely identifies the profile the Now Playing item plays from, even through app relaunches.
//let MPNowPlayingInfoPropertyIsLiveStream: String
//A number that denotes whether the Now Playing item is a live stream.
//let MPNowPlayingInfoPropertyMediaType: String
//The media type of the Now Playing item.
//let MPNowPlayingInfoPropertyPlaybackProgress: String
//The current progress of the Now Playing item.
//let MPNowPlayingInfoPropertyPlaybackRate: String
//The playback rate of the Now Playing item.
//let MPNowPlayingInfoPropertyPlaybackQueueCount: String
//The total number of items in the app’s playback queue.
//let MPNowPlayingInfoPropertyPlaybackQueueIndex: String
//The index of the Now Playing item in the app’s playback queue.
//let MPNowPlayingInfoPropertyServiceIdentifier: String
//The service provider associated with the Now Playing item.
