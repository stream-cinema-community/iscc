//
//  MenuButtonPress.swift
//  StreamCinema.atv
//
//  Created by SCC on 16/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit


final class MenuUIPress : UIPress {
  weak var view: UIView?
    
  init(view: UIView) {
    self.view = view
  }
    
    public override var timestamp: TimeInterval {
    Date().timeIntervalSince1970
  }
    
    public override var phase: UIPress.Phase {
    UIPress.Phase.began
  }
    
    public override var type: UIPress.PressType {
    UIPress.PressType.menu
  }
    
    public override var window: UIWindow? {
    self.view?.window
  }
    
    public override var responder: UIResponder? {
    self.view
  }
    
    public override var gestureRecognizers: [UIGestureRecognizer]? {
    return self.view?.gestureRecognizers
  }
}

//MARK: - example
class SignInViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTapGesture()
   }
   private func setUpTapGesture() {
    #if os(tvOS)
      let tap = UITapGestureRecognizer()
      tap.allowedPressTypes = [NSNumber(value: UIPress.PressType.menu.rawValue)]
      tap.addTarget(self, action: #selector(SignInViewController.sendEndMenuTap))
      view.addGestureRecognizer(tap)
    #endif
  }
  @objc func sendEndMenuTap(_ gestureRecognizer: UITapGestureRecognizer) {
    #if os(tvOS)
    Log.write("@@@@@ called on touch end 3 state= \(gestureRecognizer.state.rawValue)")
    UIApplication.shared.pressesEnded([MenuUIPress(view: view)], with: UIPressesEvent())
    #endif
  }
}
