//
//  ButtonExtensions.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

extension UIButton {
    func setFullImage(name:String, for state: UIControl.State) {
        self.setImage(UIImage(systemName: name), for: state)
        self.contentVerticalAlignment = .fill
        self.contentHorizontalAlignment = .fill
    }
}
