//
//  ConfigurationDataSource.swift
//  TVPlayer
//
//  Created by Jérémy Marchand on 29/12/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit

protocol KSSelectableCollection {
    func collectionNumberOfRows() -> Int
    func collection(_ tableView: UITableView, cellForRowAt indexPath:IndexPath) -> KSSelectableTableViewCell
    mutating func collection(didSelectRowAt indexPath: IndexPath)
    var selectedIndex: Int {get}
}
