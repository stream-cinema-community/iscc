//
//  ContinueView.swift
//  StreamCinema.atv
//
//  Created by SCC on 15/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class ContinueView: UIView {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var countDownProgress: UIProgressView!
    
    private(set) var isViewHidden: Bool = true
    
    public func setHidden(_ isHidden:Bool) {
        self.isViewHidden = isHidden
        self.isHidden = isHidden
    }
    
    public func set(countDown value: Float) {
        self.countDownProgress.setProgress(value, animated: true)
    }
    
    public func set(poster: URL?) {
        if let poster = poster {
            self.imageView.setCashedImage(url: poster, type: .emptyLoading)
        }
    }
    
    public func set(title: String?) {
        self.titleLabel.text = title
        self.titleLabel.layoutIfNeeded()
    }
}
