//
//  ResumableTimer.swift
//  StreamCinema.atv
//
//  Created by SCC on 06/02/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation

final class ResumableTimer: NSObject {

    private var timer: Timer? = Timer()
    private var callback: () -> Void

    private var startTime: TimeInterval?
    private var elapsedTime: TimeInterval?

    // MARK: Init

    init(interval: Double, callback: @escaping () -> Void) {
        self.callback = callback
        self.interval = interval
    }

    // MARK: API

    var isRepeatable: Bool = true
    var interval: Double = 0.5

    func isPaused() -> Bool {
        guard let timer = timer else { return false }
        return !timer.isValid
    }

    func start() {
        #if DEBUG
        Log.write("ResumableTimer start")
        #endif
        runTimer(interval: interval)
    }

    func pause() {
        #if DEBUG
        Log.write("ResumableTimer pause")
        #endif
        elapsedTime = Date.timeIntervalSinceReferenceDate - (startTime ?? 0.0)
        timer?.invalidate()
    }

    func resume() {
        #if DEBUG
        Log.write("ResumableTimer resume")
        #endif
        interval -= elapsedTime ?? 0.0
        runTimer(interval: interval)
    }

    func invalidate() {
        #if DEBUG
        Log.write("ResumableTimer invalidate")
        #endif
        timer?.invalidate()
    }

    func reset() {
        #if DEBUG
        Log.write("ResumableTimer reset")
        #endif
        startTime = Date.timeIntervalSinceReferenceDate
        runTimer(interval: interval)
    }

    // MARK: Private

    private func runTimer(interval: Double) {
        startTime = Date.timeIntervalSinceReferenceDate

        timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: isRepeatable) { [weak self] _ in
            self?.callback()
        }
    }
}
