//
//  SpeedTest.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine



protocol SpeedTestServicing: AnyObject {
    var lastSpeed: Double { get }

    func testSpeed() -> AnyPublisher<(Int, Double), Error>
}


final class SpeedTest: SpeedTestServicing {

    enum SpeedTestError: Error {
        case noLinkInformation
        case noToken
    }

    private let wsFileService: WSFile

    init(wsFileService: WSFile) {
        self.wsFileService = wsFileService
    }

    var lastSpeed: Double {
        get {
            return UserDefaults.standard.double(forKey: "NetworkSpeed")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "NetworkSpeed")
            UserDefaults.standard.synchronize()
        }
    }
    
    public var lastSpeedString: String {
        get {
            return String(format: (lastSpeed > 5) ? "%.0f Mbps" : "%.1f Mbps", lastSpeed)
        }
    }

    func testSpeed() -> AnyPublisher<(Int, Double), Error> {
      guard let token = LoginManager.tokenHash,
            let uuid = UIDevice.current.identifierForVendor else {
                return Fail(error: SpeedTestError.noToken).eraseToAnyPublisher()
      }
       return wsFileService.getFile(wsToken: token, ident: "Pv0gnYZvo6", uuid: uuid)
            .flatMap { data -> AnyPublisher<(Int, Double), Error> in
                guard let link = data.link, let url = URL(string: link) else {
                    return Fail(error: SpeedTestError.noLinkInformation).eraseToAnyPublisher()
                }
                return self.checkForSpeedTest(url: url)
            }.eraseToAnyPublisher()
    }

    private func checkForSpeedTest(url: URL)  -> AnyPublisher<(Int, Double), Error>{

        testDownloadSpeedWithTimout(url: url, timeout: 20.0)
            .handleEvents(receiveOutput: { [weak self] _, speed in
                self?.lastSpeed = speed
                Log.write("Download Speed: \(speed)")
            }, receiveCompletion: { completion in
                guard case .failure(let error) = completion else { return }
                Log.write("Speed Test Error: \(error.localizedDescription)")
            })
            .eraseToAnyPublisher()
    }


    private func testDownloadSpeedWithTimout(url: URL, timeout: TimeInterval) -> AnyPublisher<(Int, Double), Error> {

        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForResource = timeout

        let startTime = CFAbsoluteTimeGetCurrent()

        return URLSession(configuration: configuration)
            .dataTaskPublisher(for: url)
            .map { data, response -> (Int, Double) in
                let bytes = data.count
                let stopTime = CFAbsoluteTimeGetCurrent()
                let elapsed = stopTime - startTime
                let speed = elapsed != 0 ? Double(bytes) / elapsed : -1 // Bytes/s
                let convertedSpped = speed * 8 / 1024.0 / 1024.0 //convert to Mbit/s
                return (bytes, convertedSpped)
            }
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }

//    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
//        bytesReceived! += data.count
//        stopTime = CFAbsoluteTimeGetCurrent()
//    }
//
//    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
//        let elapsed = stopTime - startTime
//        if let aTempError = error as NSError?, aTempError.domain != NSURLErrorDomain && aTempError.code != NSURLErrorTimedOut && elapsed == 0  {
//            speedTestCompletionBlock?(nil, error)
//            return
//        }
//        var speed = elapsed != 0 ? Double(bytesReceived) / elapsed : -1 // Bytes/s
//        speed = speed * 8 / 1024.0 / 1024.0 //convert to Mbit/s
//        speedTestCompletionBlock?(speed, nil)
//    }
}
