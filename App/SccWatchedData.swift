//
//  WatchedData.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import CoreData
import TraktKit

public struct SccWatched {
    var isSeen: Bool
    var time: Double
    var lastUpdated: Date?
    
    var traktID: String?
    
    //Movie
    var movieID: String?
    
    //TV Show
    var tvShowID: String?
    var seasonNumber: Int?
    var seasonID: String?
    var episodeNumber: Int?
    var episodeID: String?
    var isFromTrakt: Bool = false
    
    var isTvShow: Bool {
        get {
            if self.episodeID != nil ||
                self.seasonID != nil ||
                self.tvShowID != nil {
                return true
            }
            return false
        }
    }
    
    var state:WatchedState {
        get {
            if self.isFromTrakt {
                return .trakt
            }
            if self.isSeen {
                return .done
            } else if time > 1 {
                return .paused
            } else if time > 0 {
                return .paused
            }
            return .none
        }
    }
    init(isSeen: Bool, time: Double, lastUpdated: Date?,
         traktID: String?, movieID: String?, tvShowID: String?,
         sessionNumber: Int?, seasonID: String?, episodeNumber: Int?,
         episodeID: String?)
    {
        self.isSeen = isSeen
        self.time = time
        self.lastUpdated = lastUpdated
        self.traktID = traktID
        self.movieID = movieID
        self.tvShowID = tvShowID
        self.seasonNumber = sessionNumber
        self.seasonID = seasonID
        self.episodeNumber = episodeNumber
        self.episodeID = episodeID
    }
    
    
    init(with data:NSManagedObject) {
        self.isSeen = data.value(forKey: "isWatched") as? Bool ?? false
        self.time = data.value(forKey: "time") as? Double ?? 0
        self.traktID = data.value(forKey: "traktID") as? String
        self.movieID = data.value(forKey: "movieID") as? String
        self.tvShowID = data.value(forKey: "tvShowID") as? String
        self.seasonNumber = data.value(forKey: "sessionNumber") as? Int
        self.seasonID = data.value(forKey: "seasonID") as? String
        self.episodeNumber = data.value(forKey: "episodeNumber") as? Int
        self.episodeID = data.value(forKey: "episodeID") as? String
        self.lastUpdated = data.value(forKey: "lastUpdated") as? Date
    }
    
    public func stop( progress: Float) {
        if TraktManager.sharedManager.isSignedIn {
            var movie:SyncId? = nil
            var episode:SyncId? = nil
            
            if self.isTvShow,
               let traktEpisode = self.traktID, let id = Int(traktEpisode) {
                episode = SyncId(trakt: id)
                
            } else if let traktMovie = self.traktID, let id = Int(traktMovie) {
                movie = SyncId(trakt: id)
            }
            
            let scrobble = TraktScrobble(movie: movie, episode: episode, progress: progress, appVersion: "2", appDate: Date().toDateString)
            _ = try? TraktManager.sharedManager.scrobbleStop(scrobble) { result in
                switch result {
                case .success(object: let object):
                    print(object)
                case .error(error: let error):
                    Log.write(error?.localizedDescription ?? "TraktManager TraktScrobble stop error")
                }
            }
        }
    }
    
    public func update( progress: Float) {
        if TraktManager.sharedManager.isSignedIn {
            var movie:SyncId? = nil
            var episode:SyncId? = nil
            
            if self.isTvShow,
               let traktEpisode = self.traktID, let id = Int(traktEpisode) {
                episode = SyncId(trakt: id)
                
            } else if let traktMovie = self.traktID, let id = Int(traktMovie) {
                movie = SyncId(trakt: id)
            }
            
            let scrobble = TraktScrobble(movie: movie, episode: episode, progress: progress, appVersion: "2", appDate: Date().toDateString)
            _ = try? TraktManager.sharedManager.scrobbleStart(scrobble, completion: { result in
                switch result {
                case .success(object: let object):
                    print(object)
                case .error(error: let error):
                    Log.write(error?.localizedDescription ?? "TraktManager TraktScrobble update error")
                }
            })
        }
    }
    
    public func updateObject(_ watched: NSManagedObject)  {
        watched.setValue(self.isSeen, forKey: "isWatched")
        watched.setValue(self.time, forKey: "time")
        watched.setValue(Date(), forKey: "lastUpdated")
        watched.setValue(self.traktID, forKey: "traktID")
        watched.setValue(self.movieID, forKey: "movieID")
        watched.setValue(self.tvShowID, forKey: "tvShowID")
        watched.setValue(self.seasonNumber, forKey: "sessionNumber")
        watched.setValue(self.seasonID, forKey: "seasonID")
        watched.setValue(self.episodeNumber, forKey: "episodeNumber")
        watched.setValue(self.episodeID, forKey: "episodeID")
    }
    
    static func empty() -> SccWatched {
        return SccWatched(isSeen: false, time: 0, lastUpdated: nil, traktID: nil, movieID: nil, tvShowID: nil, sessionNumber: nil, seasonID: nil, episodeNumber: nil, episodeID: nil)
    }
    
}

//MARK: - CoreData
extension SccWatched {
    func save(isWithoutUpdateTrakt: Bool = false) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let context = appDelegate.persistentContainer.viewContext
        
        let tvShowID = self.episodeID ?? self.seasonID ?? self.tvShowID
        
        if self.isWatched(by: tvShowID, isTvShow: self.isTvShow, context: context) {
            self.updateWatched(context: context)
        } else {
            self.save(context: context)
        }

        if TraktManager.sharedManager.isSignedIn,
           isWithoutUpdateTrakt == false,
           self.isSeen {
            var movies:[AddToHistoryId] = []
            let shows:[AddToHistoryId] = []
            let seasons:[AddToHistoryId] = []
            var episodes:[AddToHistoryId] = []
            
            if self.isTvShow,
               let traktEpisode = self.traktID, let id = Int(traktEpisode) {
                let hID = AddToHistoryId(trakt: id, watchedAt: Date())
                episodes.append(hID)
                
            } else if let traktMovie = self.traktID, let id = Int(traktMovie) {
                let hID = AddToHistoryId(trakt: id, watchedAt: Date())
                movies.append(hID)
            }
            
            _ = try? TraktManager.sharedManager.addToHistory(movies: movies, shows: shows, seasons: seasons, episodes: episodes, completion: { result in
                switch result {
                case .success(object: let object):
                    print(object)
                case .error(error: let error):
                    Log.write(error?.localizedDescription ?? "TraktManager addToHistory save error")
                }
            })
        }
        
    }
    
    private func updateWatched(context:NSManagedObjectContext) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
        
        if let movieID = self.movieID {
            request.predicate = NSPredicate(format: "movieID = %@", movieID)
        } else if let episodeID = self.episodeID {
            request.predicate = NSPredicate(format: "episodeID = %@", episodeID)
        } else if let tvShowID = self.tvShowID {
            request.predicate = NSPredicate(format: "tvShowID = %@", tvShowID)
        } else if let seasonID = self.seasonID {
            request.predicate = NSPredicate(format: "seasonID = %@", seasonID)
        } else {
            return
        }
        
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if let objectUpdate = result[0] as? NSManagedObject {
                self.updateObject(objectUpdate)
            } else {
                return
            }
            
            try context.save()
            
        } catch let error {
            print(error)
        }
    }
    
    private func save(context:NSManagedObjectContext) {
        let watched = NSEntityDescription.insertNewObject(forEntityName: "SccWatches",
                                                          into: context)
        self.updateObject(watched)
        do {
            try context.save()
            print("Success")
        } catch {
            Log.write("save context : \(error)")
        }
    }
    
    private func isWatched(by id:String?, isTvShow: Bool, context:NSManagedObjectContext) -> Bool {
        guard let id = id else { return false }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
        if isTvShow {
            request.predicate = NSPredicate(format: "episodeID = %@ OR seasonID == %@ OR tvShowID == %@", id,id,id)
        } else {
            request.predicate = NSPredicate(format: "movieID = %@", id)
        }
        
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count == 0 {
                return false
            }
            let data = result[0] as! NSManagedObject
            
            var movieID = data.value(forKey: "movieID") as? String
            if isTvShow {
                movieID = data.value(forKey: "episodeID") as? String
                
                if movieID == nil {
                    movieID = data.value(forKey: "seasonID") as? String
                }
                if movieID == nil {
                    movieID = data.value(forKey: "tvShowID") as? String
                }
            }
            if let movieID = movieID,
                movieID == id{
              return true
            }
        } catch {
           
        }
        return false
    }
}


final class SccWatchedDataa {

    private static func getContext() -> NSManagedObjectContext? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.viewContext
    }
    
    private static func getContainer() -> NSPersistentStoreCoordinator? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.persistentStoreCoordinator
    }

    static func getSeasonWatched(by id:String?) -> [SccWatched] {
        if let managedContext = SccWatchedDataa.getContext(),
            let id = id {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            request.predicate = NSPredicate(format: "seasonID = %@", id)
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                var watcheds:[SccWatched] = []
                for data in result as! [NSManagedObject] {
                    watcheds.append(SccWatched(with: data))
                }
                return watcheds
            } catch {
               
            }
        }
        return []
    }
    
    static func getTvShowWatched(by id:String?) -> [SccWatched]? {
        if let managedContext = SccWatchedDataa.getContext(),
            let id = id {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            request.predicate = NSPredicate(format: "tvShowID = %@", id)
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                var watcheds:[SccWatched] = []
                for data in result as! [NSManagedObject] {
                    watcheds.append(SccWatched(with: data))
                }
                return watcheds
            } catch {
               
            }
        }
        return nil
    }
    
    static func getWatched(traktID:Int) -> SccWatched? {
        if let managedContext = SccWatchedDataa.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            request.predicate = NSPredicate(format: "traktID = %@", "\(traktID)")
            
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                for data in result as! [NSManagedObject] {
                    if let trakt = data.value(forKey: "traktID") as? Int,
                       trakt == traktID {
                        let watched = SccWatched(with: data)
                        return watched
                    }
              }
            } catch {
               
            }
        }
        return nil
    }
    
    static func getWatched(by id:String?, isTvShow:Bool = false) -> SccWatched? {
        if let managedContext = SccWatchedDataa.getContext(),
            let id = id {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            var key = "movieID"
            if isTvShow {
                key = "tvShowID"
            }
            request.predicate = NSPredicate(format: "\(key) = %@", id)
            
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                for data in result as! [NSManagedObject] {
                    if let movieID = data.value(forKey: key) as? String,
                        movieID == id {
                        
                        let watched = SccWatched(with: data)
                        return watched
                    }
              }
            } catch {
               
            }
        }
        return nil
    }
    
    static func episodeWatched(by id:String) -> SccWatched? {
        if let managedContext = SccWatchedDataa.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            request.predicate = NSPredicate(format: "episodeID = %@", id)
            
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                let data = result[0] as! NSManagedObject
                return SccWatched(with: data)
            } catch {
               
            }
        }
        return nil
    }
    
    static func getAllWatched(by id:String, completion: @escaping (Result<[SccWatched], Error>) -> Void)  {
        if let managedContext = SccWatchedDataa.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            request.predicate = NSPredicate(format: "movieID = %@", id)
            request.returnsObjectsAsFaults = false
            var watches:[SccWatched] = []
            do {
                let result = try managedContext.fetch(request)
                for data in result as! [NSManagedObject] {
                    if let movieID = data.value(forKey: "movieID") as? String,
                        movieID == id {
                        let watched = SccWatched(with: data)
                        watches.append(watched)
                    }
              }
            } catch let error {
                completion(.failure(error))
            }
            completion(.success(watches))
        } else {
            completion(.failure(-1 as! Error))
        }
    }
    
    static func getAllWatched(type: FilterType, completion: @escaping (Result<[SccWatched], Error>) -> Void) {
        if let managedContext = SccWatchedDataa.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            
            var predicateString = "movieID != nil"
            if type == .tvshow {
               predicateString = "tvShowID != nil"
            }
            request.predicate = NSPredicate(format: predicateString)
            request.returnsObjectsAsFaults = false
                do {
                    var watches:[SccWatched] = try SccWatchedDataa.fetch(from: managedContext, with: request)
                    watches.sort { (leftW, rightW) -> Bool in
                        guard let leftLast = leftW.lastUpdated, let rightLast = rightW.lastUpdated else { return false }
                        return  leftLast > rightLast
                    }
                    let filtredResult = watches.filterDuplicates { (lhs, rhs) -> Bool in
                        if type == .movie {
                            return lhs.movieID == rhs.movieID
                        } else if type == .tvshow {
                            return lhs.tvShowID == rhs.tvShowID
                        }
                        return false
                    }
                    completion(.success(filtredResult))
                } catch let error {
                    completion(.failure(error))
                }
        }
    }
    
    private static func fetch(from context:NSManagedObjectContext, with request: NSFetchRequest<NSFetchRequestResult>) throws -> [SccWatched] {
        var watches:[SccWatched] = []
        let result = try context.fetch(request)
          for data in result as! [NSManagedObject] {
              let watched = SccWatched(with: data)
              watches.append(watched)
        }
        return watches
    }
    
    public static func deleteAllData(_ entity:String = "SccWatches") {
        if let managedContext = self.getContext(),
            let persistentContainer = self.getContainer() {
            
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

            do {
                try persistentContainer.execute(deleteRequest, with: managedContext)
            } catch let error as NSError {
                Log.write("Detele all data in \(entity) error : \(error.localizedDescription)")
            }
        }
    }
}

extension InfoData {
    public func getMovieWatched(isSeen:Bool = false, timeSeen:Date? = Date(), type:String = "movie") -> SccWatched {
        if let movieID = self.id {
            var sccWatched = SccWatched.empty()
            sccWatched.isSeen = isSeen
            sccWatched.lastUpdated = timeSeen
            sccWatched.traktID = self.source?.services?.trakt
            if type == "movie" {
                sccWatched.movieID = movieID
            } else if type == "tvshow" {
                sccWatched.tvShowID = movieID
            }
            
            return sccWatched
        }
        fatalError()
    }
}


//MARK: - new access
//
//final class WatchedSccData: WatchedObject {
//    static func getMovie(_ movieID: Int, entity: WatchedEntity) -> SCCMovie? {
//        <#code#>
//    }
//    
//    static func getEpisode(_ episodeID: Int, entity: WatchedEntity) -> SCCMovie? {
//        <#code#>
//    }
//    
//    static func getShow(_ showID: Int, entity: WatchedEntity) -> SCCMovie? {
//        <#code#>
//    }
//    
//    static func getAllEpisodes(for seasonID: Int?, entity: WatchedEntity) -> [SCCMovie] {
//        <#code#>
//    }
//    
//    
//}
